/**
 * 学习目标：JSX的使用注意事项-三个
 */
import React from 'react';
import ReactDOM from 'react-dom';

const divNode = (
  // * 1. JSX必须有根标签
  // 👍 <></> <React.Fragment></React.Fragment>
  <>
    <ul>
      <li>1</li>
      <li>2</li>
      <li>3</li>
    </ul>
    <ul>
      <li>1</li>
      <li>2</li>
      <li>3</li>
    </ul>

    {/*  * 2. JSX必有闭合标记 */}
    <input type="text" id="box" />

    {/*
     3. 关键字冲突：
     *  3.1 class => className
     *  3.2 for => htmlFor */}
    <label htmlFor="box">点我等同于点了input，激活input</label>
  </>
);

ReactDOM.render(divNode, document.getElementById('root'));
