/**
 * 学习目标：样式处理-行内样式
 * 语法： style= {{css属性名：属性值}}
 */
import React from 'react';
import ReactDOM from 'react-dom';

const divNode = (
  <>
    {/* 💥 React中不支持css属性连字符， 👍要求使用驼峰 */}
    {/* 💥 px单位可以省略，值直接写数字 */}
    <h1 style={{ color: 'red', fontSize: 12 }}>我想红</h1>
  </>
);

ReactDOM.render(divNode, document.getElementById('root'));
