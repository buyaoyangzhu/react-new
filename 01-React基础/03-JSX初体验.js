/*
  学习目标：JSX初体验
  步骤：
     1. 
     2. 
     3. 
*/

import React from 'react';
import ReactDOM from 'react-dom';

// const liNode1 = React.createElement('li', null, '橘子');
// const liNode2 = React.createElement('li', null, '橘子');
// const liNode3 = React.createElement('li', null, '橘子');

// const ulNode = React.createElement('ul', { className: 'list' }, liNode1, liNode2, liNode3);

// jS文件中使用html创建元素
const ulNode = (
  <ul>
    <li>橘子</li>
    <li>苹果</li>
    <li>香蕉</li>
  </ul>
);

ReactDOM.render(ulNode, document.getElementById('root'));
