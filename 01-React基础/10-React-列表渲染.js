/**
 * 学习目标：使用React的列表渲染 - 数组渲染 ，类似vue中的v-for
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

const list = ['萝卜', '白菜', '橘子'];

const listNode = list.map((item, index) => {
  return <h1 key={index}>{item}</h1>;
});

const divNode = (
  <>
    {listNode}
    {list.map((item, index) => {
      return (
        <h1
          // 💥key的作用：和v-for的key作用一模一样，都是提高更新效率
          // 🔔key的口诀：有id 用id ，没id用索引
          key={index}
        >
          {item}
        </h1>
      );
    })}
  </>
);

ReactDOM.render(divNode, document.getElementById('root'));
