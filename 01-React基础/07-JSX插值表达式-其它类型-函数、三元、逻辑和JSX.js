/**
 * 学习目标：JSX中插值表达式的使用 - 其它类型
 * 表达式：可以放在等号右边的
 * 引用类型：数组 对象
 */
import React from 'react';
import ReactDOM from 'react-dom';

// JS中的数据类型只有6中
// string number null undefined boolean object
// array  function  都属于对象类型
function hello() {
  return 'hello React';
}

const isShow = true;

const str = isShow ? 'show' : '不显示';

const divNode = (
  <div>
    {/* 1. 💥 函数本身也属于对象，函数本身不能放在插值表达式中 */}
    {/* 通常放函数的调用，函数的返回值也不能对象 */}
    {/* {hello()} */}

    {/*  三元 */}

    <h2>{isShow ? 'show' : '不显示'}</h2>

    {/* 逻辑 */}
    <h3>{isShow && '显示'}</h3>

    {/* 💥 JSX本身也可以当做表达式放在插值符号中，往往配合其它的使用方式 */}
    <h4>{<span>123</span>}</h4>
  </div>
);

ReactDOM.render(divNode, document.getElementById('root'));

// 函数本身也属于对象
