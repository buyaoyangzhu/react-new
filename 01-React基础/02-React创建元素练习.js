/*
  学习目标：React创建元素-练习
  步骤：
     1. 
     2. 
     3. 
*/

import React from 'react';
import ReactDOM from 'react-dom';

// 注意：💥使用className替代class
const title = React.createElement('div', { id: 'box', className: 'demo' }, '123');
// ❌ const title = React.createElement('div', { id: 'box', class: 'demo' }, '123');

const liNode1 = React.createElement('li', null, '橘子');
const liNode2 = React.createElement('li', null, '橘子');
const liNode3 = React.createElement('li', null, '橘子');

// 💥 React不会做html字符串解析
const ulNode = React.createElement('ul', { className: 'list' }, liNode1, liNode2, liNode3);

ReactDOM.render(ulNode, document.getElementById('root'));
