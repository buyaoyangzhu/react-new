/**
 * 学习目标：JSX中插值表达式的使用 -基础数据类型
 * 表达式：可以放在等号右边的
 * 其它：函数、三元 、 逻辑  JSX本身
 */
import React from 'react';
import ReactDOM from 'react-dom';

// * 变量 string number
const title = 'hello 73';

const divNode = (
  <div>
    {title}
    {/* 🔔// string number 正常显示 */}
    <h1>{'hello React'}</h1>
    <h1>{73}</h1>

    {/* null undefined  boolean  🔔不显示*/}
    <h2>
      {null} - {String(null)}
    </h2>
    <h2>
      {undefined} - {String(undefined)}
    </h2>
    <h3>{true}</h3>
    <h3>{false}</h3>
  </div>
);

ReactDOM.render(divNode, document.getElementById('root'));
