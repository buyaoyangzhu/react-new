/**
 * 学习目标：React中的条件渲染 类似v-if
 */
import React from 'react';
import ReactDOM from 'react-dom';

const isLoading = false;
// 显示加载中
// 正常数据显示

// 1. if else语句 往往配合函数的返回值
function loadData() {
  if (isLoading) {
    return <h1>加载中...</h1>;
  } else {
    return <div>加载完毕</div>;
  }
}

// 2. 三元表达式
const divNode = (
  <>
    {loadData()}

    {isLoading ? <h1>加载中...</h1> : <h3>加载完毕</h3>}

    {/* 3. 逻辑运算符 */}
    {isLoading && <h1>加载中...</h1>}
    {!isLoading && <div>加载完毕</div>}
  </>
);

ReactDOM.render(divNode, document.getElementById('root'));
