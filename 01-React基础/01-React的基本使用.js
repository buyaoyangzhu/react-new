/*
  学习目标：使用React创建h1标签、显示在浏览器中
*/

// 1. 导入react react-dom
import React from 'react';
// React负责：创建元素 -虚拟DOM
import ReactDOM from 'react-dom';
// ReactDOM负责： 渲染React元素

// 2. 创建React元素
// 语法：React.createElement("标签的类型", {标签的属性名: 属性值}, 标签的内容)
// 💥 不需要给标签设置任何属性，可以使用null 或者 {}
const title = React.createElement('i', {}, 'hello React');
console.log('title  ----->  ', title);

// 3. 渲染React元素
// 语法： ReactDOM.render(react元素， 真实的DOM节点做挂载点)
// 💥 root必须是一个真正的dom节点
ReactDOM.render(title, document.getElementById('root'));
