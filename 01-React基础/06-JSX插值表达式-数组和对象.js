/**
 * 学习目标：JSX中插值表达式的使用 -引用数据类型
 * 表达式：可以放在等号右边的
 * 引用类型：数组 对象 函数
 * 其它：三元 、 逻辑  JSX本身
 */
import React from 'react';
import ReactDOM from 'react-dom';
// 对象
const person = {
  type: 'zs',
  props: {
    title: '123',
  },
};

//  数组
const list = [<div>1</div>, <div>2</div>, <div>3</div>];

const divNode = (
  <div>
    {/* 💥对象不能直接放在插值表达式中，会报错 */}
    {person.type}

    {/* 🔔 数组中的每项元素，都会当做一个dom节点渲染出来 */}
    <h1>{list}</h1>
    <h1>123</h1>
  </div>
);

ReactDOM.render(divNode, document.getElementById('root'));

// 对象

// 数组
