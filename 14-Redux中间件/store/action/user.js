import { USER_UPDATE } from './actionType';

export const updateUserAction = (payload) => ({
  type: USER_UPDATE,
  payload,
});
