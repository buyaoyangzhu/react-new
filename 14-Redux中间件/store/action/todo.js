import { TODO_ADD, TODO_CHECKALL, TODO_DEL, TODO_UPDATE } from './actionType';

export const updateByIdAction = (id) => {
  return {
    type: TODO_UPDATE,
    payload: id,
  };
};

export const delByIdAction = (id) => {
  return {
    type: TODO_DEL,
    payload: id,
  };
};

function loadNewTodoAPI() {
  const newTodo = {
    task: '异步返回的任务',
    id: Date.now(),
    isDone: false,
  };
  console.log('开始请求');
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('请求成功，2秒后,数据返回');
      resolve(newTodo);
    }, 2000);
  });
}
export const addAction = () => {
  return async (dispatch) => {
    const newTask = await loadNewTodoAPI();

    dispatch({ type: TODO_ADD, payload: newTask });
  };
};

export const checkAllAction = (checked) => ({
  type: TODO_CHECKALL,
  payload: checked,
});
