import { TODO_ADD, TODO_CHECKALL, TODO_DEL, TODO_UPDATE } from '../action/actionType';

const initialState = {
  list: [
    { id: 1, task: '学习Vue', isDone: true },
    { id: 2, task: '学习Vue3', isDone: true },
    { id: 3, task: '学习React', isDone: false },
  ],
  type: 'all',
};

export default function todoReducer(state = initialState, { type, payload }) {
  console.warn(' 进入仓库了 ----->  ', type, payload);

  switch (type) {
    case TODO_UPDATE:
      return {
        ...state,
        list: state.list.map((item) => {
          return { ...item, isDone: item.id === payload ? !item.isDone : item.isDone };
        }),
      };
    case TODO_DEL:
      return {
        ...state,
        list: state.list.filter((item) => item.id !== payload),
      };
    case TODO_ADD:
      return {
        ...state,
        list: [payload, ...state.list],
      };
    // 3.
    case TODO_CHECKALL:
      return {
        ...state,
        list: state.list.map((item) => {
          return { ...item, isDone: payload };
        }),
      };
    default:
      return state;
  }
}
