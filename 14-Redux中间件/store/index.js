/*
  学习目标：配置redux调试工具，观察效果
  步骤：
     1. 下包 npm i redux-devtools-extension
     2. 配置
     3. chrome装插件
     4. 重启浏览器
*/

import { applyMiddleware, legacy_createStore as createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './reducer';
// 口诀： 工具 》 应用 》 中间件
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;
