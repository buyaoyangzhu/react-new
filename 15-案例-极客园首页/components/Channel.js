import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getChannelsAction, updateIdAction } from '../store/action/channel';

export default function Channel() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getChannelsAction());
  }, [dispatch]);

  const { channelList, currentId } = useSelector((state) => state.channel);
  return (
    <ul className="category">
      {channelList.map((item) => (
        <li
          onClick={() => dispatch(updateIdAction(item.id))}
          key={item.id}
          className={item.id === currentId ? 'select' : ''}
        >
          {item.name}
        </li>
      ))}
    </ul>
  );
}
