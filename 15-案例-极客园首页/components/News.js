/*
  学习目标：取出数据，渲染新闻列表
*/

import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getNewsByIdAction } from '../store/action/news';
import defaultSrc from '../assets/defaultCover.jpg';

export default function News() {
  const dispatch = useDispatch();
  const { currentId } = useSelector((state) => state.channel);
  const { newsList } = useSelector((state) => state.news);

  useEffect(() => {
    dispatch(getNewsByIdAction(currentId));
  }, [dispatch, currentId]);

  return (
    <div className="list">
      {/* 2. 列表渲染 */}
      {newsList.map((item) => {
        // 💥 cover.type为0是，images字段不存在
        const images = item.cover.images || [];
        const imgSrc = images[0] || defaultSrc;

        return (
          <div key={item.art_id} className="article_item">
            <h3 className="van-ellipsis">{item.title}</h3>
            <div className="img_box">
              <img src={imgSrc} className="w100" alt="" />
            </div>
            <div className="info_box">
              <span>{item.aut_name}</span>
              <span>{item.comm_count}评论</span>
              <span>{item.pubdate}</span>
            </div>
          </div>
        );
      })}
    </div>
  );
}
