import Channel from './components/Channel';
import News from './components/News';

export default function App() {
  return (
    <>
      <Channel />
      <News />
    </>
  );
}
