import { getChannelsAPI } from '../../api/channel';
import { CHANNEL_SAVE_LIST, CHANNEL_UPDATE_ID } from './actionType';

export const getChannelsAction = (payload) => {
  return async (dispatch) => {
    const res = await getChannelsAPI();
    dispatch({ type: CHANNEL_SAVE_LIST, payload: res.data.channels });
  };
};

export const updateIdAction = (id) => ({
  type: CHANNEL_UPDATE_ID,
  payload: id,
});
