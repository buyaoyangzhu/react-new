import { getNewsByIdAPI } from '../../api/news';
import { NEWS_SAVE_LIST } from './actionType';

export const getNewsByIdAction = (channel_id) => {
  return async (dispatch) => {
    const res = await getNewsByIdAPI(channel_id);
    dispatch({ type: NEWS_SAVE_LIST, payload: res.data.results });
  };
};
