// 频道的type
export const CHANNEL_UPDATE_ID = 'channel/updateCurrentId';
export const CHANNEL_SAVE_LIST = 'channel/saveList';

// 新闻的type
export const NEWS_SAVE_LIST = 'news/saveList';
