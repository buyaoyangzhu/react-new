import { NEWS_SAVE_LIST } from '../action/actionType';

const initialState = {
  newsList: [],
};

export default function newsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case NEWS_SAVE_LIST:
      return {
        ...state,
        newsList: payload,
      };
    default:
      return state;
  }
}
