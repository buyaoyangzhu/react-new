import { CHANNEL_SAVE_LIST, CHANNEL_UPDATE_ID } from '../action/actionType';

const initialState = {
  channelList: [],
  currentId: 0,
};

export default function channelReducer(state = initialState, { type, payload }) {
  switch (type) {
    case CHANNEL_SAVE_LIST:
      return {
        ...state,
        channelList: payload,
      };
    case CHANNEL_UPDATE_ID:
      return {
        ...state,
        currentId: payload,
      };
    default:
      return state;
  }
}
