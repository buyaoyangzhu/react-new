import { combineReducers } from 'redux';
import channelReducer from './channel';
import newsReducer from './news';

const rootReducer = combineReducers({
  channel: channelReducer,
  news: newsReducer,
});

export default rootReducer;
