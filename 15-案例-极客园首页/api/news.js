import request from '../utils/request';

export const getNewsByIdAPI = (id) => {
  return request({
    url: '/v1_0/articles',
    params: {
      channel_id: id,
      timestamp: Date.now(),
    },
  });
};
