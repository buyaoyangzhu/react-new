/*
  学习目标：性能优化-shouldComponentUpdate控制是否更新

*/
import React from 'react';

export default class App extends React.Component {
  state = {
    count: 0,
    msg: 'hello ',
  };
  render() {
    const { count, msg } = this.state;
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={() => this.setState({ count: count + 1 })}>点我修改count</button>
        <button onClick={() => this.setState({ msg: msg + '~' })}>点我修改msg</button>
        <Child count={count}></Child>
      </div>
    );
  }
}

class Child extends React.Component {
  // nextProps代表更新后的props对象，this.props代表更新前的props对象
  // 比较props的属性值,来决定是否更新
  shouldComponentUpdate(nextProps) {
    if (nextProps.count === this.props.count) {
      return false;
    } else {
      return true;
    }
  }

  componentDidUpdate() {
    console.log('componentDidUpdate 执行了  ----->  ');
  }

  render() {
    const { count } = this.props;
    return <div>Child - {count}</div>;
  }
}
