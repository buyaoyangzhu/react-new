/*
  学习目标：setState更新过程
  1. 💥setState更新是异步的，
  2. 💥setState连续调用会产生合并效果：最后一个setState会覆盖前方所有的setState
  设计目的：减少频繁的操作dom，减少更新次数

*/

import React, { Component } from 'react';

export default class App extends Component {
  state = {
    count: 0,
  };

  handleAdd = () => {
    this.setState({ count: this.state.count + 1 });
    this.setState({ count: this.state.count + 2 });
    this.setState({ count: this.state.count + 3 });
  };

  render() {
    const { count } = this.state;
    console.log('render触发了，开始渲染  ----->  ');
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={this.handleAdd}>点我看控制台</button>
      </div>
    );
  }
}
