/*
  学习目标：React更新机制
  1. 默认情况下：父组件更新，所有的后代组件全部更新
  2. 不会影响兄弟组件
*/

import React, { Component } from 'react';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Son></Son>
        <Brother></Brother>
      </div>
    );
  }
}

class Son extends Component {
  state = {
    count: 0,
  };
  render() {
    const { count } = this.state;
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={() => this.setState({ count: count + 1 })}>点我修改count</button>
        <Child></Child>
      </div>
    );
  }
}

class Brother extends React.Component {
  componentDidUpdate() {
    console.log('Brother被更新了  ----->  ');
  }

  render() {
    return <div>Brother</div>;
  }
}

class Child extends React.Component {
  componentDidUpdate() {
    console.log('Child被更新了  ----->  ');
  }

  render() {
    return <div>Child</div>;
  }
}
