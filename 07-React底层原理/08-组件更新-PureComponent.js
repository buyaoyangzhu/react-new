/*
  学习目标：性能优化-
*/
import React from 'react';

export default class App extends React.Component {
  state = {
    count: 0,
    msg: 'hello ',
    title: '',
    name: '',
  };
  render() {
    const { count, msg, title, name } = this.state;
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={() => this.setState({ count: count + 1 })}>点我修改count</button>
        <button onClick={() => this.setState({ msg: msg + '~' })}>点我修改msg</button>
        <Child count={count} title={title} name={name}></Child>
      </div>
    );
  }
}

// PureComponent 是React内置一个纯组件
// 特点：内置了shouldComponentUpdate，会自动比较所有的props是否改变，是否更新组件
// 💥 不要滥用PureComponent，
class Child extends React.PureComponent {
  // shouldComponentUpdate(nextProps) {
  //   if (nextProps.count === this.props.count) {
  //     return false;
  //   } else {
  //     return true;
  //   }

  // }

  componentDidUpdate() {
    console.log('componentDidUpdate 执行了  ----->  ');
  }

  render() {
    const { count } = this.props;
    return <div>Child - {count}</div>;
  }
}
