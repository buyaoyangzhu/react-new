/*
  学习目标：setState的第二种写法,， （还是异步的语法）
  语法： setState( (旧的state) => 返回的新的state)
  效果：合并：setState会等待上一次的更新结果，再去计算
  目的：减少render执行的次数，
*/

import React, { Component } from 'react';

export default class App extends Component {
  state = {
    count: 0,
  };

  handleAdd = () => {
    this.setState((preState) => {
      return {
        count: preState.count + 1,
      };
    });

    this.setState((preState) => {
      return {
        count: preState.count + 2,
      };
    });

    this.setState((preState) => {
      return {
        count: preState.count + 3,
      };
    });
  };

  render() {
    const { count } = this.state;
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={this.handleAdd}>点我看控制台</button>
      </div>
    );
  }
}
