/*
  学习目标：setState的第三种写法：第二个参数的使用
  💥缺点：1. 增加回调地域 2. 不会合并，没有减少render执行次数，频繁操作dom
  👎早期写法，工作中几乎不用。
*/

import React, { Component } from 'react';

export default class App extends Component {
  state = {
    count: 0,
  };

  handleAdd = () => {
    this.setState({ count: this.state.count + 1 }, () => {
      this.setState({ count: this.state.count + 2 }, () => {
        this.setState({ count: this.state.count + 3 });
      });
    });
  };

  render() {
    const { count } = this.state;
    console.log('render再次执行了  ----->  ');
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={this.handleAdd}>点我看控制台</button>
      </div>
    );
  }
}
