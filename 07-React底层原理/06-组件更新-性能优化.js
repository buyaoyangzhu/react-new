/*
  学习目标：性能优化-原则
  1. 功能第一
  2. 优化第二

  优化手段：
    1. 减轻state的设计： 非渲染的数据，不放在state中
    2. 避免无效更新：shouldComponentUpdate


  更新阶段执行顺序：
  shouldComponentUpdate : 更新前 ⬇️
  render： 更新中                ⬇️
  componentDidUpdate ： 更新后   ⬇️
*/

import React, { Component } from 'react';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Son></Son>
      </div>
    );
  }
}

class Son extends Component {
  state = {
    count: 0,
  };
  render() {
    const { count } = this.state;
    return (
      <div>
        <h1>{count}</h1>
        <button onClick={() => this.setState({ count: count + 1 })}>点我修改count</button>
        <Child></Child>
      </div>
    );
  }
}

// 避免不必要的更新

class Child extends React.Component {
  shouldComponentUpdate() {
    console.log('shouldComponentUpdate 执行了  ----->  ');
    // 返回true表示更新
    return true;

    // 返回false表示不更新
    // return false;
  }

  componentDidUpdate() {
    console.log('componentDidUpdate 执行了  ----->  ');
  }

  render() {
    console.log('render 执行了  ----->  ');
    return <div>Child</div>;
  }
}
