/*
  学习目标：性能优化-
*/
import React from 'react';

export default class App extends React.Component {
  state = {
    list: [1, 2, 3],
  };

  handleClick = () => {
    // ✅ [..this.state.list] 表示新的数组，内存地址已变
    this.setState({ list: [...this.state.list, 4] });
    // ❌ 浅比较：会导致list的内存地址，一直没有变化。旧版得React中会导致隐藏bug
    // this.state.list.push(4);
    // this.setState(this.state);
  };
  render() {
    const { list } = this.state;
    return (
      <div>
        <button onClick={this.handleClick}>点我修改list</button>
        <Child list={list}></Child>
      </div>
    );
  }
}

class Child extends React.PureComponent {
  shouldComponentUpdate(nextProps) {
    console.log('this.props.list  ----->  ', this.props.list);
    console.log('nextProps.list  ----->  ', nextProps.list);
    console.log('nextProps.list === this.props.list  ----->  ', nextProps.list === this.props.list);
    if (nextProps.list === this.props.list) {
      return false;
    } else {
      // 代表更新
      return true;
    }
  }

  render() {
    const { list } = this.props;
    return <h1>Child - {list}</h1>;
  }
}
