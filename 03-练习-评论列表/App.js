/*
  学习目标：
  需求1：✅准备数据、渲染评论列表
  需求2：✅根据id，删除一条数据
  需求3：✅实现发布评论 - 受控组件改造
  需求4：✅实现发布评论
  需求5：✅实现清除评论功能
  
*/

import React from 'react';
import './index.css';

export default class App extends React.Component {
  state = {
    // 用户名输入框
    username: '',
    // 评论内容
    content: '',

    list: [
      {
        id: 1,
        username: '思聪',
        content: '想你的夜',
      },
      {
        id: 2,
        username: '马芸',
        content: '我对钱不感兴趣',
      },
      {
        id: 3,
        username: '王简林',
        content: '实现一个小目标',
      },
    ],
  };

  // 2.1. 定义函数的函数
  handleDelById(id) {
    // 2.3. 调用setState去删除一条数据
    const list = this.state.list.filter((item) => item.id !== id);
    this.setState({ list });
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    // 3.4 对象的键名插值定义，复用同一个handleChange
    this.setState({ [name]: value });
  };

  // 4.1 定义一个新增方法
  handleAdd = (e) => {
    // 4.3 阻止默认行为
    e.preventDefault();

    const { username, content, list } = this.state;
    // 4.4 处理边界问题：非空判断
    if (!username.trim() || !content.trim()) {
      alert('评论人，或评论内容不能为空');
      return;
    }

    // 4.5 setState添加新内容到list中
    // 4.6 清空输入框的值
    const newObj = { username, content, id: Date.now() };
    this.setState({ list: [newObj, ...list], username: '', content: '' });
  };

  // 5.1 定义清除list的方法
  handleClear = () => {
    this.setState({ list: [] });
  };

  render() {
    const { list, username, content } = this.state;
    return (
      <div className="app">
        {/* 表单提交的时候，运行发布评论的函数 */}
        <form onSubmit={this.handlePublish}>
          <input
            className="user"
            type="text"
            placeholder="请输入评论人"
            // 3.1 state控制表单的value或者checked
            value={username}
            // 3.2 onChange事件配合setState修改状态
            onChange={this.handleChange}
            // 3.3 设置name
            name="username"
          />
          <br />
          <textarea
            value={content}
            className="content"
            cols="30"
            rows="10"
            placeholder="请输入评论内容"
            // 3.3 设置name
            name="content"
            onChange={this.handleChange}
          />
          <br />
          <button
            // 4.2 绑定click事件
            onClick={this.handleAdd}
          >
            发表评论
          </button>
          <button onClick={this.handleClear} type="button">
            清空评论
          </button>
        </form>
        {/* 3. 使用length做条件渲染 */}
        {!list.length ? (
          <div>暂无数据</div>
        ) : (
          <ul>
            {list.map((item) => (
              <li key={item.id}>
                <h3>评论人: {item.username}</h3>
                <p>评论内容：{item.content}</p>
                <button
                  // 2.2. 绑定事件，并且传参id
                  onClick={() => this.handleDelById(item.id)}
                >
                  删除
                </button>
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}
