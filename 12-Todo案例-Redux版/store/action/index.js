export const updateByIdAction = (id) => {
  return {
    type: 'update',
    payload: id,
  };
};

export const delByIdAction = (id) => {
  return {
    type: 'del',
    payload: id,
  };
};

export const addAction = (newTaskStr) => {
  const newTask = { id: Date.now(), isDone: false, task: newTaskStr };
  return {
    type: 'add',
    payload: newTask,
  };
};

export const checkAllAction = (checked) => ({
  type: 'checkAll',
  payload: checked,
});
