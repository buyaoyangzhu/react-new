const initialState = {
  list: [
    { id: 1, task: '学习Vue', isDone: true },
    { id: 2, task: '学习Vue3', isDone: true },
    { id: 3, task: '学习React', isDone: false },
  ],
  type: 'all',
};

export default function todoReducer(state = initialState, { type, payload }) {
  console.warn(' 进入仓库了 ----->  ', type, payload);

  switch (type) {
    case 'update':
      return {
        ...state,
        list: state.list.map((item) => {
          return { ...item, isDone: item.id === payload ? !item.isDone : item.isDone };
        }),
      };
    case 'del':
      return {
        ...state,
        list: state.list.filter((item) => item.id !== payload),
      };
    case 'add':
      return {
        ...state,
        list: [payload, ...state.list],
      };
    // 3.
    case 'checkAll':
      return {
        ...state,
        list: state.list.map((item) => {
          return { ...item, isDone: payload };
        }),
      };
    default:
      return state;
  }
}
