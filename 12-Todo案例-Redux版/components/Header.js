import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addAction } from '../store/action';

export default function Header() {
  const [task, setTask] = useState('');
  const dispatch = useDispatch();

  const handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      dispatch(addAction(task));
      setTask('');
    }
  };

  return (
    <header className="header">
      <h1>todos</h1>
      <input
        value={task}
        onChange={(e) => setTask(e.target.value)}
        onKeyDown={handleKeyDown}
        className="new-todo"
        placeholder="需要做什么"
        autoFocus
      />
    </header>
  );
}
