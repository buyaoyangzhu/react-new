import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { checkAllAction, delByIdAction, updateByIdAction } from '../store/action';

export default function Main() {
  const { list } = useSelector((state) => state);

  const dispatch = useDispatch();

  console.log('组件被刷新了  ----->  ', list);
  const isAll = list.length ? list.every((item) => item.isDone) : false;

  return (
    <section className="main">
      <input
        id="toggle-all"
        className="toggle-all"
        type="checkbox"
        checked={isAll}
        onChange={() => dispatch(checkAllAction(!isAll))}
      />
      <label htmlFor="toggle-all">全选</label>
      <ul className="todo-list">
        {list.map((item) => {
          return (
            <li key={item.id} className={item.isDone ? 'completed' : ''}>
              <div className="view">
                <input
                  onChange={() => dispatch(updateByIdAction(item.id))}
                  className="toggle"
                  type="checkbox"
                  checked={item.isDone}
                />
                <label>{item.task}</label>
                <button
                  className="destroy"
                  onClick={() => dispatch(delByIdAction(item.id))}
                ></button>
              </div>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
