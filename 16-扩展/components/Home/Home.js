/*
  学习目标：css-module 总结
  注意：
    1. css  less  sass中都可以使用css-module
    2. React脚手架集成了sass、css-module可以直接使用
 
*/
import styles from './index.module.scss';

export function Home() {
  return (
    <div
      // 3. 顶层的标签，仍使用点语法，获取类名
      className={styles.home}
    >
      <h1
        // 4. 其它的标签，仍使用字符串类名
        className="size-100"
      >
        我是Home
      </h1>
      <h2 className="box">我是100字号</h2>
    </div>
  );
}
