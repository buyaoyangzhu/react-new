/*
  学习目标：了解React中的样式冲突
*/

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Home } from './components/Home/Home';
import { Main } from './components/Main/Main';

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/home" component={Home}></Route>
        <Route path="/main" component={Main}></Route>
      </Switch>
    </Router>
  );
}
