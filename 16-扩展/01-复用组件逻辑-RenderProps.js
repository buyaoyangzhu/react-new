/*
  学习目标：React中复用组件逻辑的三种方式- Render Props
  需求：Main和Footer组件，复用监听鼠标位置的逻辑，显示鼠标的位置
  步骤：
     1. 💥组件内封装状态(数据)和方法（修改数据的）
     2. 💥通过props.render属性可以是一个函数，
     3. 由render函数返回jsx，自定义如何渲染
*/

import React, { useState } from 'react';

export default function App() {
  return (
    <div>
      <LogMouse
        render={(mouse, handleMoseMove) => (
          <div onMouseMove={handleMoseMove}>
            {mouse.x} - {mouse.y}
          </div>
        )}
      />

      <LogMouse
        render={(mouse, handleMoseMove) => (
          <div onMouseMove={handleMoseMove}>{JSON.stringify(mouse)}</div>
        )}
      />
      <LogMouse
        render={(mouse, handleMoseMove) => <Header mouse={mouse} handeMouseMose={mouse} />}
      />
    </div>
  );
}

function LogMouse({ render }) {
  const [mouse, setMouse] = useState({ x: 0, y: 0 });
  const handleMoseMove = (e) => {
    setMouse({ x: e.clientX, y: e.clientY });
  };

  return <>{render(mouse, handleMoseMove)}</>;
}

function Header() {
  return <h1>我是Header</h1>;
}
