/*
  学习目标：React中复用组件逻辑的三种方式- 自定义hooks
*/

import React, { useState } from 'react';

export default function App() {
  return (
    <div>
      <Header></Header>
      <Main></Main>
    </div>
  );
}

// 1. 定义一个hooks: hooks内封装逻辑
// 💥 hooks就一个函数，名字要以useXxxx开头
// 💥 自定义hooks内，可以调用其它的hooks
function useMouse() {
  const [mouse, setMouse] = useState({ x: 0, y: 0 });

  const handleMouseMouse = (e) => {
    setMouse({ x: e.clientX, y: e.clientY });
  };

  return { mouse, handleMouseMouse };
}
// 2. 在其它组件内调用hooks
function Header() {
  const { mouse, handleMouseMouse } = useMouse();

  return (
    <h1 onMouseMove={handleMouseMouse}>
      x-{mouse.x}
      <span>y-{mouse.y}</span>
    </h1>
  );
}

function Main() {
  const { mouse, handleMouseMouse } = useMouse();
  console.log('mouse  ----->  ', mouse);
  return (
    <div onMouseMove={handleMouseMouse}>
      <h1>我是Main组件</h1>
      <h2>{mouse.x}</h2>
      <h2>{mouse.y}</h2>
    </div>
  );
}
