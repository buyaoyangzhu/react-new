/*
  学习目标：React中复用组件逻辑的三种方式- HOC
  HOC全名： 高阶函数、高阶组件
  本质：hoc是一个函数，内置一套组件逻辑、返回一个新的租金
  关键点：
    1. 定义一个函数
    2. 函数内，封装一个组件
    3. 函数接收一个组件，通过父传子技术，复用逻辑
*/

import React, { useState } from 'react';
const MyHeader = showMose(Header);
const MyMain = showMose(Main);

export default function App() {
  return (
    <div>
      <MyHeader></MyHeader>

      <MyMain></MyMain>
    </div>
  );
}

// 1. 定义一个函数
function showMose(Component) {
  // 2. 函数内，封装一个组件
  // 💥记得return 返回内置的组件
  return function LogMouse() {
    const [mouse, setMouse] = useState({ x: 0, y: 0 });
    const handleMoseMove = (e) => {
      setMouse({ x: e.clientX, y: e.clientY });
    };

    return (
      <div onMouseMove={handleMoseMove}>
        {/* 3. 通过父传子复用逻辑， 返回接收的组件 */}
        <Component mouse={mouse}></Component>
      </div>
    );
  };
}

function Header({ mouse }) {
  return (
    <h1>
      我是Header组件 - {mouse.x} -{mouse.y}
    </h1>
  );
}

function Main({ mouse }) {
  return (
    <div>
      <h1>我是Main组件</h1>
      <i>x={mouse.x}</i>
      <i>y={mouse.y}</i>
    </div>
  );
}
