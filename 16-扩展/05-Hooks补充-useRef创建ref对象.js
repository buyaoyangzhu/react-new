/*
  学习目标：Hooks补充-useRef
  步骤：
     1. useRef
     2. useHistory
     3. useLocation
*/

import React, { useRef } from 'react';

// 需求：使用useRef创建ref对象
// ref对象作用：1. 获取dom元素

export default function App() {
  // 1. 创建ref
  const iptRef = useRef();
  console.log('iptRef  ----->  ', iptRef);

  const handleClick = () => {
    console.log('iptRef  ----->  ', iptRef);
    // 3. ref对象.current获取dom元素
    iptRef.current.focus();
  };

  return (
    <div>
      <input
        type="text"
        // 2. 绑定给dom元素
        ref={iptRef}
      />
      <button onClick={handleClick}>点我激活iput</button>
    </div>
  );
}
