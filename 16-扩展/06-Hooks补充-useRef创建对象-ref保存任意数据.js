/*
  学习目标：Hooks补充-useRef-保存任意（不需要渲染的）数据
*/

import React, { useEffect, useRef, useState } from 'react';

// ref对象作用
// 1. 获取dom元素
// 2. 组件实例对象(函数式组件没有实例对象，只有类组件才有)
// 💥3. 可以保存(任意不需要渲染的）数据

export default function App() {
  const [isShow, setIsShow] = useState(true);
  return <div>{isShow && <Child></Child>}</div>;
}

function Child() {
  const timerIdRef = useRef();

  // 需求:1 挂载后开启定时器
  useEffect(() => {
    timerIdRef.current = setInterval(() => {
      console.log('123  ----->  ', 123);
    }, 100);
  }, []);

  useEffect(() => {
    // 需求: 2卸载时清除定时器
    return () => {
      console.log('timerIdRef  ----->  ', timerIdRef);
      clearInterval(timerIdRef.current);
    };
  }, []);

  return (
    <div>
      <input type="text" />
    </div>
  );
}
