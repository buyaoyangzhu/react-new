/*
  学习目标：Hooks-补充 - useHistory 
  useHistory: 👍
      作用：获取history对象
  useLocation: 👍
      作用：获取location 对象

  与props.history的区别：
  1. 只有设置过Route匹配规则的组件，才有props.history， 一般用在class组件中
  2. 👍useHistory（）可以在任意的函数组件中，获取history对象
*/

import React from 'react';

import { BrowserRouter as Router, Route, Switch, useHistory, useLocation } from 'react-router-dom';

export default function App() {
  return (
    <Router>
      <Header></Header>
      <Switch>
        <Route path="/home" component={Home}></Route>
      </Switch>
    </Router>
  );
}

function Home() {
  return <h1>我是Home</h1>;
}

function Header() {
  const history = useHistory();

  const location = useLocation();
  console.log('location  ----->  ', location);
  return (
    <h2>
      Header
      <button onClick={() => history.push('/test')}>点我跳转</button>
    </h2>
  );
}
