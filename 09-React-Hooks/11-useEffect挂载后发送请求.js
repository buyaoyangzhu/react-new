/*
  学习目标：使用useEffect和axios获取数据，渲染数据
  步骤：
     1. npm i axios
     2. 封装requst.js
     3. 查文档、封装请求的函数
     4. 声明state list
     5. 使用useEffect触发请求
*/

import React, { useEffect, useState } from 'react';
import { getChannelsAPI } from './api/channel';

export default function App() {
  const [list, setList] = useState([]);
  const loadData = async () => {
    const res = await getChannelsAPI();
    setList(res.data.channels);
  };

  // ✅ 封装一个异步函数
  useEffect(() => {
    loadData();
  }, []);

  // ❌ useEffect接受一个同步函数，不要把回调函数改为异步的
  // useEffect(async () => {
  //   const res = await getChannelsAPI();
  //   setList(res.data.channels);
  // }, []);
  return (
    <div>
      <ul>
        {list.map((item) => {
          return <li key={item.id}>{item.name}</li>;
        })}
      </ul>
    </div>
  );
}
