/*
  学习目标：受控组件-hooks
*/

import React, { useState } from 'react';

export default function App() {
  // 1. 声明state
  const [value, setValue] = useState('');

  // 3. 声明方法用const
  const handleChange = (e) => {
    setValue(e.target.value);
  };

  return (
    <div>
      <input
        type="text"
        // 2. 控制value
        value={value}
        // 💥 绑定方法没有this了
        onChange={handleChange}
      />
    </div>
  );
}
