/*
  学习目标：useEffect使用第二个参数，模拟生命周期
    需求1： 模拟挂载时
    需求2： 模拟更新时

*/

import React, { useState, useEffect } from 'react';

export default function App() {
  const [count, setCount] = useState(50);

  // 💥注意：useEffect是函数，函数可以调用多次，谁也不影响

  // 1. 第二个参数为数组：空数组时代表的是挂载时。等价于componentDidMount
  useEffect(() => {
    console.log('我是挂载时的钩子函数，只会执行一次  ----->  ');
    document.title = count;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // 2. 第二个参数为数组：数组内有依赖项（状态）时，
  // 💥代表的是挂载时和更新时二合一， 类似Vue中的watch属性，开启了immediately
  // 💥等价于componentDidMount 和 componentDidUpdate 二合一
  // useEffect(() => {
  //   console.log('我是更新时的钩子函数  ----->  ');
  //   document.title = count;
  // }, [count]);

  return (
    <div>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>点我</button>
    </div>
  );
}
