/*
  学习目标：useEffect第二个参数使用总结

*/

import React, { useState, useEffect } from 'react';

export default function App() {
  const [count, setCount] = useState(50);
  const [msg, setMsg] = useState('hello');
  const [msg2, setMsg2] = useState('hello');
  const [msg3, setMsg3] = useState('hello');
  const [msg4, setMsg4] = useState('hello');
  const [msg5, setMsg5] = useState('hello');
  const [msg6, setMsg6] = useState('hello');
  const [msg7, setMsg7] = useState('hello');

  // 1. []
  // 作用：模拟挂载时的生命周期
  // useEffect(() => {
  //   console.log('我是挂载时的钩子函数，只会执行一次  ----->  ');
  //   document.title = count;
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  // 2. [依赖项]
  // 作用： 模拟挂载和更新，二合一的； 类似Vue中的watch属性，开启了immediately
  // 特点：只有依赖项变化时，才触发，相当于只监听依赖项
  // useEffect(() => {
  //   console.log('我是count更新时的钩子函数  ----->  ');
  //   document.title = count;
  // }, [count]);

  // 3.👎 不写第二个参数
  // 特点：会监听所有状态的变化
  // 原因：造成性能浪费
  useEffect(() => {
    console.log('我是所有更新时的钩子函数  ----->  ');
    document.title = count;
  });

  return (
    <div>
      <h1>{count}</h1>
      <h1>{msg}</h1>
      <button onClick={() => setCount(count + 1)}>点我</button>
      <button onClick={() => setMsg(msg + '~')}>点我修改msg</button>
    </div>
  );
}
