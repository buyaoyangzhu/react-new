import React, { useState } from 'react';
/*
  学习目标：useState使用的三个注意事项和一个规范写法
  步骤：
     1. useState可以多次调用，声明多个状态
     2. useState可声明任意数据类型
     3. 更新状态，还是新值覆盖旧值

*/
export default function App() {
  // 1. useState可以多次调用，声明多个状态
  const [count, setCount] = useState(100);
  const [msg, setMsg] = useState('hello React');

  // 2. useState可声明任意数据类型
  const [list, setList] = useState([1, 2, 3]);
  const [person, setPerson] = useState({ name: 'zs', age: 18 });

  // 👍 const [状态， set驼峰变量名] = useState(初始值)
  const [hello, setHello] = useState('hello');
  const [msg2, setMsg2] = useState('123');
  return (
    <div>
      <h1>{count}</h1>
      <h2>{msg}</h2>
      <h3>{list}</h3>
      <h4>{JSON.stringify(person)}</h4>

      <button
        onClick={() => {
          // 3. 更新状态，还是新值覆盖旧值
          setPerson({ ...person, name: 'ww' });
          // ❌person.name = '王五';
        }}
      >
        点我更新person
      </button>
    </div>
  );
}
