/*
  学习目标：useEffect的综合写法
 
*/
import React, { useEffect, useState } from 'react';

export default function App() {
  const [isShow, setIsShow] = useState(true);
  return (
    <div>
      {isShow && <Child></Child>}
      <button onClick={() => setIsShow(!isShow)}>点击卸载子组件</button>
    </div>
  );
}

function Child() {
  // 1. 不同的逻辑用不同的useEffect分开写
  useEffect(() => {
    const timerId = window.setTimeout(() => {
      console.log('一秒时间到，爆炸');
    }, 1000);

    return () => {
      window.clearTimeout(timerId);
    };
  }, []);

  useEffect(() => {
    const fn = () => {
      console.log('浏览器窗口被调整了');
    };
    // 2. 同一个逻辑的挂载和卸载，合在一个useEffect里起来写
    window.addEventListener('resize', fn);

    return () => {
      window.removeEventListener('resize', fn);
    };
  }, []);

  return <div>我是子组件</div>;
}
