/*
  学习目标：useEffect可以监听多个状态的变化
  需求：将count + msg方法title上
*/

import React, { useState, useEffect } from 'react';

export default function App() {
  const [count, setCount] = useState(50);
  const [msg, setMsg] = useState('hello');

  // 1. 💥 useEffect的第二个参数，可以同时有多个依赖项
  // 表示同时监听多个状态的变化
  // 场景： 需要根据多个状态的值，进行计算
  // useEffect(() => {
  //   document.title = count + msg;
  // }, [count, msg]);

  // 2. 分开监听
  // 场景：不需要使用多个状态计算
  useEffect(() => {
    console.log('count  ----->  ', count);
  }, [count]);

  useEffect(() => {
    console.log('msg  ----->  ', msg);
  }, [msg]);

  return (
    <div>
      <h1>{count}</h1>
      <h1>{msg}</h1>
      <button onClick={() => setCount(count + 1)}>点我</button>
      <button onClick={() => setMsg(msg + '~')}>点我修改msg</button>
    </div>
  );
}
