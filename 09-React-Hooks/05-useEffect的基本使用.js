/*
  学习目标：使用useEffect修改网页的标题
  需求：声明一个状态count， 将count的值实时同步到title
  步骤：
     1. useState声明count
     2. 点击按钮，修改count
     3. 使用useEffect实时修改title
*/

import React, { useState, useEffect } from 'react';

export default function App() {
  // 1.
  const [count, setCount] = useState(100);

  // 3.1 导入useEffect钩子函数: 会自动执行
  // 语法： useEffect(回调函数)；  回调函数中可以获取到更新后的值
  useEffect(() => {
    document.title = count;
  });
  return (
    <div>
      <h1>{count}</h1>
      {/* 2. */}
      <button onClick={() => setCount(count + 1)}>点我</button>
    </div>
  );
}
