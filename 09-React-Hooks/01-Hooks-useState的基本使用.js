/*
  学习目标：使用useState声明状态
  语法： const [状态变量，更新状态的函数] useState(初始值)
  步骤：
     1. 导入 useState钩子函数： 声明状态
     2. 调用函数
     3. 解构数组，获取：状态变量，和更新状态的函数
*/

// rfc

// 1. 导入 useState钩子函数： 声明状态
import React, { useState } from 'react';

export default function App() {
  // 2. 调用钩子函数
  const [count, setCount] = useState(100);
  // count 等价于 this.state.couont
  // setCount等价于this.setState({couont: xxx})
  return (
    <div>
      <h1>{count}</h1>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        点我加+1
      </button>
    </div>
  );
}
