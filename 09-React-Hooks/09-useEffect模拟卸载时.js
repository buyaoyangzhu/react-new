/*
  学习目标：使用useEffect模拟卸载时的生命周期
  需求：挂载后开启监听窗口的改变事件、卸载时取消监听的事件
. 
*/

import React, { useEffect, useState } from 'react';

export default function App() {
  // 准备组件的显示与隐藏
  const [isShow, setIsShow] = useState(true);
  return (
    <div>
      {isShow && <Child></Child>}
      <button onClick={() => setIsShow(!isShow)}>点击卸载子组件</button>
    </div>
  );
}

function Child() {
  // 在子组件中挂载时监听窗口改变事件
  const scrollFn = () => {
    console.log('窗口大小改变了  ----->  ');
  };
  useEffect(() => {
    // 1、
    window.addEventListener('resize', scrollFn);

    // 语法： useEffect回调函数内，返回一个函数，这个函数就是卸载时会自动执行的钩子函数
    // 通过返回一个函数，模拟卸载时的生命周期
    // 2.
    return () => {
      console.log('我要被卸载掉了  ----->  ');
      window.removeEventListener('resize', scrollFn);
    };
  }, []);

  return <div>我是子组件</div>;
}
