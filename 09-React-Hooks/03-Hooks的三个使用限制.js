import React, { useState } from 'react';
/*
  学习目标：hooks的三个使用限制
  1. hooks不能放在if语句中
  2. 不能放在for语句中


*/

const list = [];

export default function App() {
  console.log('App函数重新执行了  ----->  ');
  if (false) {
    // . hooks不能放在if语句中
    // 💥React 是根据useState调用顺序，保证每个状态的值分配。React要求调用顺序永远不变
    // const [hello, setHello] = useState('123');
  }

  for (let index = 0; index < list.length; index++) {
    // 2. 不能放在for语句中
    // const [hello, setHello] = useState('123');
  }
  const [count, setCount] = useState(100);
  const [msg, setMsg] = useState('123');

  return (
    <div>
      <h1>{count}</h1>

      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        点我更新person
      </button>
    </div>
  );
}

// React中非普通函数：
// 1. 函数式组件: Xxxx
// 2. 自定义钩子函数： useXxx

// 自定义hooks
function useXxxx(params) {
  const [state, setState] = useState('');
}

// 函数组件
function Header() {
  const [state, setState] = useState('xxxx');
  return null;
}

// 普通函数：非React组件、非useXxx开头的函数
function hello(params) {
  // 3. 普通函数中不能使用hooks
  // const [state, setState] = useState('xxxx');
  return null;
}
