/**
 * 学习目标：React绑定一个click事件
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  render() {
    console.log('this  ----->  ', this);
    return (
      <div>
        <button
        // onClick={function () {
        //   alert(123);
        // }}

        // 语法： on+大写驼峰事件名 = {函数名}

        // 两种常用的写法
        // 👍1. 少量代码: 使用一个箭头函数
        //✅ onClick={() => alert('123')}

        // ✅onClick={this.handleClick}
        //常见错误的写法
        // 1. 不能加()：❌ onClick={this.handleClick()}
        //  事件必须接受一个函数，而且要放在花括号中
        // 2. 不能放在字符串中：❌onClick="alert('123')"
        >
          点我
        </button>
      </div>
    );
  }

  // 2. 👍多行代码，自定义一个函数
  handleClick() {
    console.log('helloi React  ----->  ');
    console.log('helloi React  ----->  ');
    console.log('helloi React  ----->  ');
    console.log('helloi React  ----->  ');
    console.log('helloi React  ----->  ');
    console.log('helloi React  ----->  ');
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
