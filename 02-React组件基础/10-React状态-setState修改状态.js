/**
 * 学习目标：使用setState修改count，每次+100
 * setState的作用：更新state，驱动界面变化
 * 语法：setState({更新的数据： 新的值})
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  // ins
  state = {
    count: 99,
    msg: '123',
  };
  render() {
    return (
      <div>
        <button onClick={this.handleAdd}>点我count + 100</button>
        <h1>{this.state.count}</h1>
        <h1>{this.state.msg}</h1>
      </div>
    );
  }

  handleAdd = () => {
    // console.log('this.setState  ----->  ', this.setState);
    // 使用setState修改count，每次+100
    // ✅this.setState({ });
    this.setState({ msg: this.state.msg + '123', count: this.state.count + 100 });

    // React中不可变数据-不允许直接修改state
    // ❌
    // this.state.count += 100;

    // ❌
    // this.setState({ count: (this.state.count += 100) });
  };
}

ReactDOM.render(<App></App>, document.getElementById('root'));
