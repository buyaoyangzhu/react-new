/**
 * 学习目标：React事件中this的指向问题 - 了解即可，不推荐
 * 原理：bind改变this为render函数中的this
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    count: 100,
  };

  render() {
    return (
      <div>
        <button
          // 👎bind 改变this指向问题
          onClick={this.handleClick1.bind(this)}
        >
          点我访问count
        </button>
      </div>
    );
  }

  handleClick1() {
    console.log('this  ----->  ', this);
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
