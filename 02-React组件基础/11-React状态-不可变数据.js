/**
 * 学习目标：概念 - 不可变数据
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    list: [1, 2, 3, 4],
  };
  render() {
    // cs
    const { list } = this.state;
    return (
      <div>
        <ul>
          {list.map((item) => {
            return <li key={item}>{item}</li>;
          })}
        </ul>
        <button onClick={this.handleAdd}>点我添加5</button>
      </div>
    );
  }

  handleAdd = () => {
    // 不可变数据： 不能使用会修改原数据的方法
    // 如：push 、concat
    // 如：this.state.count++

    // ✅setState 新值覆盖旧值
    this.setState({ list: [...this.state.list, 5] });
    // ❌ this.state.list.push(5);
  };
}

ReactDOM.render(<App></App>, document.getElementById('root'));
