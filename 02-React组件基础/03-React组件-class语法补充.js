/*
  学习目标：ES6 -class知识点补充
  class的基本语法
  extends的作用
*/

// class的作用： 创建对象
// 💥 属性 = 值

class Programer {
  gender = '男';

  writeCode() {
    console.log('我代码写的贼6  ----->  ');
  }
}

// extends的作用：基础父类（爸爸）所有的属性和方法
// 目的：复用相同的属性和方法，减少创建对象时消耗的内存
class FrontEnd extends Programer {
  //  自动获得爸爸所有的属性和方法
  name = 'fe';
}

class BackEnd extends Programer {}

const be = new BackEnd();
const fe = new FrontEnd();

console.log('fe  ----->  ', fe);

fe.writeCode();
be.writeCode();
