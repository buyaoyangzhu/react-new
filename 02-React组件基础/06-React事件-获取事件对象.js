/**
 * 学习目标：获取事件对象，阻止a标签的默认行为
 */
import React from 'react';
import ReactDOM from 'react-dom';

// inc
class App extends React.Component {
  render() {
    return (
      <div>
        <a
          // 💥 绑定事件的默认形参就是事件对象event， 类似Vue，
          // 1. onClick={(e) => e.preventDefault()}
          href="http://www.baidu.com"
          onClick={this.handleClick}
        >
          点我跳百度
        </a>
      </div>
    );
  }

  // 2. 多行代码的写法
  handleClick(e) {
    e.preventDefault();
    console.log('come here  ----->  ');
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
