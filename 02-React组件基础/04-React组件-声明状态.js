/**
 * 学习目标：组件声明状态state、使用状态
 * 作用：state用来声明数据，类似Vue中的data函数
 */
import React from 'react';
import ReactDOM from 'react-dom';

// 1. 声明class 组件
class App extends React.Component {
  // 2. 声明state - 数据
  // 语法： state = {数据变量名： 值}, 可以声明多个数据
  state = {
    count: 100,
    msg: 'hello React',
  };

  render() {
    // 3. 获取state: 通过this.state.xxxx访问数据
    // 💥 this 指向组件实例对象，与Vue类似
    console.log('this  ----->  ', this);
    return (
      <h1>
        我是App - {this.state.count} - {this.state.msg}
      </h1>
    );
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
