/**
 * 学习目标：代码优化-初级版
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    name: 'zs',
    intro: '',
    city: '4',
    isSingle: true,
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChangeSingle = (e) => {
    console.log('e.target.checked  ----->  ', e.target.checked);
    this.setState({ isSingle: e.target.checked });
  };

  render() {
    const { name, intro, city, isSingle } = this.state;
    return (
      <div>
        <div>
          姓名：
          <input name="name" type="text" value={name} onChange={this.handleChange} />
          <br />
          描述：<textarea name="intro" value={intro} onChange={this.handleChange}></textarea>
          <br />
          城市：
          <select name="city" value={city} onChange={this.handleChange}>
            <option value="1">北京</option>
            <option value="2">上海</option>
            <option value="3">广州</option>
            <option value="4">深圳</option>
          </select>
          <br />
          是否单身：
          <input type="checkbox" checked={isSingle} onChange={this.handleChangeSingle} />
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
