/**
 * 学习目标：能够给事件传值
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  render() {
    return (
      <div>
        <button
          // 2. 调用时，给函数传值
          // 完整写法
          // onClick={(e) => {
          //   this.handleClick(1, e);
          // }}

          // 简写：去掉花括号
          //  💥加一个箭头，变成函数
          onClick={(e) => this.handleClick(e, 2)}
        >
          点我传值
        </button>

        <button>点我传值2</button>
      </div>
    );
  }

  // 1. 自定义事件函数
  handleClick(e, num) {
    // 能够接收到外部传来的值
    console.log('num, e  ----->  ', num, e);
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
