/**
 * 学习目标：非受控组件和受控组件
 * 区别：
 *  1. 👍受控组件：指表单元素的值，由state控制
 *  2. 👎非受控组件：指表单元素的值，由DOM本身管理
 *
 *
 * 受控组件作用：收集用户输入的数据
 *  🔔优点：更符合数据驱动的思想，获取用户输入的数据更便捷
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    name: 'xx',
    city: '',
  };
  handleChange = (e) => {
    console.log('e.target.value  ----->  ', e.target.value);
  };

  handleSubmit = () => {
    console.log('this.state  ----->  ', this.state);
  };

  render() {
    return (
      <div>
        <input id="name" type="text" onChange={this.handleChange} />
        <input id="city" type="text" onChange={this.handleChange} />
        <input id="city1" type="text" onChange={this.handleChange} />
        <input id="city2" type="text" onChange={this.handleChange} />
        <input id="city3" type="text" onChange={this.handleChange} />
        <input id="city3" type="text" onChange={this.handleChange} />
        <input id="city4" type="text" onChange={this.handleChange} />
        <input id="city5" type="text" onChange={this.handleChange} />
        <button>点我</button>
      </div>
    );
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
