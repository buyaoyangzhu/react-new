/**
 * 学习目标：创建使用clas组件
 */
import React from 'react';
import ReactDOM from 'react-dom';

console.log('React  ----->  ', React);

// 语法：
// class  extends是关键字要写死，React.Component也要写死

// 注意：
// 创建上：
// 1. 组件名字必须大写字母开头
// 2. render函数中必须返回一段JSX，如果不需要渲染任何内容，返回null
// 3. 必须有render函数，否则报错，render函数类似函数式组件的语法

// 1. 使用上：React对大小写敏感

class Hello extends React.Component {
  render() {
    return null;
  }
}

const divNode = (
  <>
    <Hello></Hello>
    {/*❌ <hello></hello> */}
  </>
);

ReactDOM.render(divNode, document.getElementById('root'));
