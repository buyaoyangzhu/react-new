/**
 * 学习目标：练习-购物车
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    count: 0,
  };

  render() {
    return (
      <div>
        <p>当前数值为：{this.state.count}</p>
        <hr />
        <button onClick={() => this.handleClick(1)}>+1</button>
        <span>{this.state.count}</span>
        <button onClick={() => this.handleClick(-1)}>-1</button>

        <Hello2></Hello2>
      </div>
    );
  }

  handleClick = (num) => {
    console.log('num  ----->  ', num);
    const { count } = this.state;
    this.setState({ count: count + num });
  };
}

function Hello() {
  return <h1>hello</h1>;
}

const Hello2 = () => <h2>hello2</h2>;

ReactDOM.render(<App></App>, document.getElementById('root'));
