/**
 * 学习目标：常见的受控组件
 *
 * 1. 表单元素的value或checked属性，由state控制
 * 2. onChange配合setState改变数据
 *
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    name: 'zs',
    intro: '',
    city: '4',
    isSingle: true,
  };

  handleChangeName = (e) => {
    this.setState({ name: e.target.value });
  };

  handleChangeIntro = (e) => {
    this.setState({ intro: e.target.value });
  };

  handleChangeCity = (e) => {
    // console.log('e.target.value  ----->  ', e.target.value);
    this.setState({ city: e.target.value });
  };

  handleChangeSingle = (e) => {
    console.log('e.target.checked  ----->  ', e.target.checked);
    this.setState({ isSingle: e.target.checked });
  };

  render() {
    const { name, intro, city, isSingle } = this.state;
    return (
      <div>
        <div>
          姓名：
          <input type="text" value={name} onChange={this.handleChangeName} />
          <br />
          描述：<textarea value={intro} onChange={this.handleChangeIntro}></textarea>
          <br />
          城市：
          <select value={city} onChange={this.handleChangeCity}>
            <option value="1">北京</option>
            <option value="2">上海</option>
            <option value="3">广州</option>
            <option value="4">深圳</option>
          </select>
          <br />
          是否单身：
          <input type="checkbox" checked={isSingle} onChange={this.handleChangeSingle} />
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
