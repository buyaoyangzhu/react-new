/**
 * 学习目标：React组件-创建函数组件
 */
import React from 'react';
import ReactDOM from 'react-dom';

// 1. 创建：
// 1.1 组件名称首字母必须大写
// 1.2 函数必须返回一段JSX，如果不需要渲染任何内容，返回一个null

// ✅ 组件
function Hello() {
  return null;
}

// ❌表达式
function hello() {
  return <h1>我是hello</h1>;
}

const divNode = (
  <>
    {/* 使用上：💥React对大小写敏感 */}
    <Hello></Hello>

    {hello()}
  </>
);

ReactDOM.render(divNode, document.getElementById('root'));
