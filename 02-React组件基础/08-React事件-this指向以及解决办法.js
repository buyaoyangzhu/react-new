/**
 * 学习目标：React事件中this的指向问题，解决方案
 * 原理：箭头函数，根据所在的环境自动绑定this
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    count: 100,
  };

  // React自带结构体中，this指向正确: React源码内部处理过
  render() {
    // console.log('this  ----->  ', this);
    return (
      <div>
        <button
          // 解决办法1： 在render函数中，使用箭头函数绑定事件
          onClick={() => this.handleClick1()}
        >
          点我访问count
        </button>
        <button onClick={this.handlexxx}>点我访问count</button>
      </div>
    );
  }

  handleClick1() {
    console.log('this  ----->  ', this);
  }

  // 解决办法2：自定方法，改为箭头函数写法
  // React非自带结构体，this默认指向undefined
  handlexxx = () => {
    console.log('this  ----->  ', this);
  };
}

ReactDOM.render(<App></App>, document.getElementById('root'));
