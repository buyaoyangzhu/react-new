/**
 * 学习目标：使用受控组件，实现双向绑定的效果
 * 类似：vue中v-model
 * 用处：收集用户输入的内容
 * 两个关键点：
 *  1. 表单元素的值，由state控制
 *  2. onChange配合setState修改state的值
 */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  state = {
    msg: 'hello React',
  };

  render() {
    return (
      <div>
        <input
          type="text"
          value={this.state.msg}
          // 💥React对表单的事件特殊处理，以后，所有表单的改变事件都用onChange
          onChange={(e) => {
            console.log('e.target.value  ----->  ', e.target.value);
            this.setState({ msg: e.target.value });
          }}
          // 💥 React中的失焦事件为onBlur
          onBlur={(e) => {
            console.log('e.target.value  ----->  ', e.target.value);
          }}
        />
      </div>
    );
  }
}

ReactDOM.render(<App></App>, document.getElementById('root'));
