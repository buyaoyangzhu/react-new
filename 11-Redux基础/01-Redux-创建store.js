/*
  学习目标：使用Redux创建仓库store 使用getState获取数据
  步骤：
     1. npm i redux
     2. 导入createStore函数
     3. 调用函数，创建store

     4. 使用getState获取数据
*/

import { legacy_createStore as createStore } from 'redux';
// 3.💥createStore 需要接收一个函数作为参数，reducer函数
// 如何设置初始值：
// 给reducer函数的形参分配的初始值，可以是任意数据类型
const store = createStore(function (state = { count: 0, msg: 'hello React' }) {
  // 💥 并且需要返回state
  return state;
});

// 4. 通过store.getState方法回去redux中的数据
console.log('store.getState()  ----->  ', store.getState());
