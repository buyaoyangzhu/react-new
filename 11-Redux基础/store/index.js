import { legacy_createStore as createStore } from 'redux';

const initState = { count: 99, msg: 'hello React', list: [{}] };

function countReducer(state = initState, { type, payload }) {
  console.warn(' ----->  ', type, payload);
  switch (type) {
    case 'add':
      return {
        count: state.count + payload,
      };

    case 'des':
      return {
        count: state.count - payload,
      };

    default:
      return state;
  }
}

export const addAction = (num) => {
  return {
    type: 'add',
    payload: num,
  };
};

export const desAction = (num) => {
  return {
    type: 'des',
    payload: num,
  };
};

const store = createStore(countReducer);

export default store;
