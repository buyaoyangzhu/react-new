/*
  学习目标：使用subscribe监听仓库数据的变化
*/
import { legacy_createStore as createStore } from 'redux';
const store = createStore(function (state = { count: 0 }, action) {
  console.warn('action  ----->  ', action);
  if (action.type === 'add') {
    return {
      count: state.count + 1,
    };
  }

  if (action.type === 'des') {
    return {
      count: state.count - 1,
    };
  }

  return state;
});

store.dispatch({ type: 'add' });
store.dispatch({ type: 'add' });
// 1. const 解除监听的函数 = store.subscribe( 回调函数)
// 💥 监听的函数，要写在dispatch触发前，才有效
const unFn = store.subscribe(() => {
  console.log('数据变化，更新后的值是：', store.getState());
});

store.dispatch({ type: 'add' });
store.dispatch({ type: 'add' });
store.dispatch({ type: 'des' });
store.dispatch({ type: 'des' });

// 2. 调用解除监听的函数，不再监听仓库数据变化
// 💥 关闭监听后，不会再触发监听的函数
unFn();
