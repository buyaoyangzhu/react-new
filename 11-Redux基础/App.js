import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addAction, desAction } from './store';

export default function App() {
  const [show, setShow] = useState(true);
  return (
    <div>
      <h1>Redux基础案例</h1>
      <button onClick={() => setShow(!show)}>点击切换卸载挂载</button>
      {show ? <Son /> : null}
    </div>
  );
}

function Son() {
  const count = useSelector((state) => state.count);
  const dispatch = useDispatch();

  return (
    <div>
      <h2>子组件</h2>
      <h2>count:{count}</h2>
      <button onClick={() => dispatch(addAction(1))}>+1</button>
      <button onClick={() => dispatch(addAction(15))}>+15</button>
      <button onClick={() => dispatch(desAction(1))}>-1</button>
      <button onClick={() => dispatch(desAction(10))}>-10</button>
    </div>
  );
}
