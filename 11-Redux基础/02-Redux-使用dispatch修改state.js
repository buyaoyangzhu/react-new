/*
  学习目标：使用dispatch对count + 1 和减1

*/

import { legacy_createStore as createStore } from 'redux';
const store = createStore(function (state = { count: 0 }, action) {
  // 2. 通过redcuer函数的第二个参数，接收到action对象
  console.log('action  ----->  ', action);
  // 3. 通过计算返回新值，覆盖旧值
  if (action.type === 'add') {
    // 💥 通过返回新值覆盖旧值
    return {
      count: state.count + 1,
    };
  }

  if (action.type === 'des') {
    // 💥 通过返回新值覆盖旧值
    return {
      count: state.count - 1,
    };
  }

  return state;
});

// 1. 通过dispatch发起修改state的action
// 💥 dispatch必须接受一个action
// 💥 action是普通的js对象，而且必须有type字段
store.dispatch({ type: 'add' });
console.log('store.getState().count  ----->  ', store.getState().count);

store.dispatch({ type: 'des' });
console.log('store.getState().count  ----->  ', store.getState().count);
