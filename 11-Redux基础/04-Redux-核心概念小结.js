/*
  学习目标：Redux核心概念-小结
  步骤：
     1. state： 保存仓库数据
     2. dispatch：
        特点：💥redux中更新state唯一的方法，就是dispatch(Action对象)
        作用：触发一个action
     3. reducer
        作用：计算state
        不可变数据：通过新值覆盖旧值，🔔(state, action) => newState
        纯函数：固定输入、固定输出
        💥注意：发请求、获取DOM都不能写在reducer函数中
     4. action
       本质：具有type字段的JS对象
       作用：描述发生了什么
     5. store
        以上内容合起来就是strore
     注意：
      1. 不可变数据数据
      2. 单向数据流
*/
