/*
  学习目标：Redux推荐写法
    
*/
import { legacy_createStore as createStore } from 'redux';

// 2. 使用单独的变量维护初始值，通常叫initState之类
const initState = { count: 0, msg: 'hello React', list: [{}] };

// 1. 使用单独的函数维护reducer，通常叫xxxReducer
function countReducer(state = initState, action) {
  console.warn('action  ----->  ', action);
  // 3. 使用switch语句替换if语句
  switch (action.type) {
    case 'add':
      return {
        count: state.count + 1,
      };

    case 'des':
      return {
        count: state.count - 1,
      };

    default:
      // 💥 记得default代表redux初始化数据，要写
      return state;
  }
}

const store = createStore(countReducer);

store.dispatch({ type: 'add' });
store.dispatch({ type: 'add' });
const unFn = store.subscribe(() => {
  console.log('数据变化，更新后的值是：', store.getState());
});

store.dispatch({ type: 'add' });
store.dispatch({ type: 'add' });
store.dispatch({ type: 'des' });
store.dispatch({ type: 'des' });

unFn();
