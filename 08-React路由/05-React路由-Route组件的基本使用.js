/*
  学习目标：Route组件的使用说明

  步骤：
     1. 
     2. 
     3. 
*/

import React, { Component } from 'react';
import { Route, Link, NavLink, BrowserRouter as Router } from 'react-router-dom';
import './index.css';

export default class App extends Component {
  render() {
    return (
      <Router>
        <NavLink to="/home" activeClassName="xxx-xx" exact>
          首页
        </NavLink>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Link to="/friend">发现</Link>
        <div>
          <h1>
            <i id="xxx">
              {/* 
              2. Route表示挂载点和规则对象二合一，放在哪里就渲染在哪里
                如果path不匹配Route会返回一个null
              */}
              <Route
                path="/home"
                component={Home}
                // 1.默认是模糊匹配， exact表示精准匹配
                exact
              ></Route>
            </i>
          </h1>
        </div>

        {/* 3. React中默认，从上向下，依次匹配，从头匹配到尾 */}
        <Route path="/my" component={MyMusic}></Route>
        <Route path="/my" component={MyMusic}></Route>
        <Route path="/my" component={MyMusic}></Route>
        <Route path="/friend" component={Friend}></Route>

        {/* 4. Route组件匹配任意路径的两种方式 */}
        {/* 4.1 path写一个/ , 原理是模糊匹配*/}
        <Route path="/" component={Friend} exact></Route>
        {/* 4.2 表示匹配任意路径，类似vue中的通配符， 不写path，设置eaxct无效 */}
        <Route component={Friend} exact></Route>
      </Router>
    );
  }
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend() {
  return <h1>我是朋友组件</h1>;
}
