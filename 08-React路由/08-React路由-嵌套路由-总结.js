import React, { Component } from 'react';
import { Switch, Route, Link, NavLink, BrowserRouter as Router } from 'react-router-dom';
import './index.css';

export default class App extends Component {
  render() {
    return (
      <Router>
        <NavLink to="/home" activeClassName="xxx-xx" exact>
          首页
        </NavLink>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Link to="/friend">发现</Link>

        <Switch>
          <Route path="/home" component={Home} exact></Route>
          <Route path="/my" component={MyMusic}></Route>
          <Route path="/friend" component={Friend}></Route>

          <Route component={NotFound}></Route>
        </Switch>
      </Router>
    );
  }
}

// 嵌套路由的使用步骤：
// 1  新建子组件
// 2. 在父组件内，设置二级路由规则和挂载点
// 3. 👍Swtich组件包住所有的Route组件

// 注意：
// 💥 二级路由的path，从一级路径开始写完整路径
// 💥 Link组件的to属性，从一级路径开始写完整的路径

// 💥 嵌套路由中，一级路径一般不能加exact
// 💥 嵌套路由中，父子级路径时可以重复的，都会显示出来

function Friend() {
  return (
    <div>
      <h1>我是朋友组件</h1>
      <Link to="/friend">朋友1</Link>
      <br />
      <Link to="/friend/friend2">朋友2</Link>
      <br />
      <Link to="/friend/friend3">朋友3</Link>
      <Switch>
        <Route path="/friend" component={Friend1} exact></Route>
        <Route path="/friend/friend2" component={Friend2}></Route>
        <Route path="/friend/friend3" component={Friend3}></Route>
      </Switch>
    </div>
  );
}

function NotFound() {
  return <h3>404页面</h3>;
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend1() {
  return <h1>我是朋友1组件</h1>;
}
function Friend2() {
  return <h2>我是朋友2组件</h2>;
}
function Friend3() {
  return <h3>我是朋友3组件</h3>;
}
