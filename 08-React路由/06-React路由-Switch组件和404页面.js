/*
  学习目标：React中Switch组件的使用
  和404页面的设置

*/

import React, { Component } from 'react';
import { Switch, Route, Link, NavLink, BrowserRouter as Router } from 'react-router-dom';
import './index.css';

export default class App extends Component {
  render() {
    return (
      <Router>
        <NavLink to="/home" activeClassName="xxx-xx" exact>
          首页
        </NavLink>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Link to="/friend">发现</Link>

        <Switch>
          {/* Swtich组件的作用：匹配到任意一个路径，就停止匹配 */}
          {/* 👍 推荐用Swtich组件包住所有的Route */}
          <Route path="/home" component={Home} exact></Route>
          <Route path="/my" component={MyMusic}></Route>
          <Route path="/my" component={MyMusic}></Route>
          <Route path="/my" component={MyMusic}></Route>
          <Route path="/friend" component={Friend}></Route>
          {/* 
            404页面：通过Switch组件和 不写path的Route来实现。
                     🔔通常会放在最后
          */}
          <Route component={NotFound}></Route>
        </Switch>
      </Router>
    );
  }
}

function NotFound() {
  return <h3>404页面</h3>;
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend() {
  return <h1>我是朋友组件</h1>;
}
