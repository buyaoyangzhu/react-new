/*
  学习目标：使用重定向组件， /  => /home
*/

import React, { Component } from 'react';
import { Redirect, Switch, Route, Link, NavLink, BrowserRouter as Router } from 'react-router-dom';
import './index.css';

export default class App extends Component {
  render() {
    return (
      <Router>
        <NavLink to="/home" activeClassName="xxx-xx" exact>
          首页
        </NavLink>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Link to="/friend">发现</Link>

        <Switch>
          {/* 💥 一般要配合eaxct */}
          <Redirect from="/" to="/home" exact></Redirect>
          <Route path="/home" component={Home} exact></Route>
          <Route path="/my" component={MyMusic}></Route>
          <Route path="/friend" component={Friend}></Route>

          <Route component={NotFound}></Route>
        </Switch>
      </Router>
    );
  }
}

function Friend() {
  return (
    <div>
      <h1>我是朋友组件</h1>
      <Link to="/friend">朋友1</Link>
      <br />
      <Link to="/friend/friend2">朋友2</Link>
      <br />
      <Link to="/friend/friend3">朋友3</Link>
      <Switch>
        <Route path="/friend" component={Friend1} exact></Route>
        <Route path="/friend/friend2" component={Friend2}></Route>
        <Route path="/friend/friend3" component={Friend3}></Route>
      </Switch>
    </div>
  );
}

function NotFound() {
  return <h3>404页面</h3>;
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend1() {
  return <h1>我是朋友1组件</h1>;
}
function Friend2() {
  return <h2>我是朋友2组件</h2>;
}
function Friend3() {
  return <h3>我是朋友3组件</h3>;
}
