/*
  学习目标：使用React路由，实现路径跳转组件切换
  步骤：
     1. 💥npm i react-router-dom@5.3
     2. 导入组件 HashRouter Route Link
     3. 使用HashRouter 和Route组件
     4. 使用Link去跳转
*/

import React, { Component } from 'react';

import { HashRouter, Route, Link } from 'react-router-dom';

export default class App extends Component {
  render() {
    return (
      //  HashRouter负责实例化路由 ， 类似vue中的VueRouter，
      // 💥需要包住所有的代码
      <HashRouter>
        {/* 💥 to属性跳转时，需要以/开头 */}
        <Link to="/home">首页</Link>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Link to="/friend">发现</Link>
        {/* Route组件：类似Vue中的规则对象和挂载点二合一 */}
        <Route path="/home" component={Home}></Route>
        <Route path="/my" component={MyMusic}></Route>
        <Route path="/friend" component={Friend}></Route>
      </HashRouter>
    );
  }
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend() {
  return <h1>我是朋友组件</h1>;
}
