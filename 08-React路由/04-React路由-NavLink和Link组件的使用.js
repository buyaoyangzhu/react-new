/*
  学习目标：
  1. 知道Link和NavLink组件的区别
  2. 知道如何自定义高亮类名
  3. 💥知道：模糊匹配 和 精准匹配
    模糊匹配： url路径  包含 to属性或path属性
    精准匹配 url路径与to属性或path属性  一模一样
*/

import React, { Component } from 'react';
import { Route, Link, NavLink, BrowserRouter as Router } from 'react-router-dom';
import './index.css';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Link to="/home">首页</Link>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Link to="/friend">发现</Link>

        <hr />

        {/* 1. NavLink会自带高亮类名 */}
        <NavLink
          // 💥to属性要以/开头
          to="/home"
          // 2. activeClassName 用来自定义高亮类名
          activeClassName="xxx-xx"
          // 💥exact 表示只有精准匹配时，才高亮
          exact
        >
          首页
        </NavLink>
        <br />
        <NavLink to="/home/test" activeClassName="xxx-xx">
          首页
        </NavLink>

        <Route path="/home" component={Home}></Route>
        <Route path="/my" component={MyMusic}></Route>
        <Route path="/friend" component={Friend}></Route>
      </Router>
    );
  }
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend() {
  return <h1>我是朋友组件</h1>;
}
