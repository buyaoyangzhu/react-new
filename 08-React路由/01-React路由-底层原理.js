/*
  学习目标：通过React基础知识，实现路径切换，界面切换
*/

import React from 'react';

export default class App extends React.Component {
  // 3. 监听url路径的变化
  componentDidMount() {
    window.addEventListener('hashchange', () => {
      this.setState({
        currentUrl: window.location.hash.slice(1),
      });
    });
  }

  // 1. 声明state
  state = {
    currentUrl: '/home',
  };

  render() {
    const { currentUrl } = this.state;
    return (
      <div>
        <h1>app组件</h1>
        <ul>
          <li>
            <a href="#/home">首页</a>
          </li>
          <li>
            <a href="#/my">我的音乐</a>
          </li>
          <li>
            <a href="#/friend">我的朋友</a>
          </li>
        </ul>
        {/* 2. 做条件渲染，同时只能显示一个组件 */}
        {currentUrl === '/home' && <Home></Home>}
        {currentUrl === '/my' && <MyMusic></MyMusic>}
        {currentUrl === '/friend' && <Friend></Friend>}
      </div>
    );
  }
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend() {
  return <h1>我是朋友组件</h1>;
}
