/*
  学习目标：Router组件的使用说明
*/

import React, { Component } from 'react';

// 👍 将BrowserRouter 或HashROuter通过as 重命名为Router

import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

export default class App extends Component {
  render() {
    return (
      // 💥Route必须放在Router之内
      // 💥Router 负责实例化路由，整个项目中只使用一次
      // 👍使用Router组件包住所有的代码
      <Router>
        <Link to="/home">首页</Link>
        <br />
        <Link to="/my">我的音乐</Link>
        <br />
        <Route path="/home" component={Home}></Route>
        <Link to="/friend">发现</Link>
        <Route path="/my" component={MyMusic}></Route>
        <Route path="/friend" component={Friend}></Route>
      </Router>
    );
  }
}

function Home() {
  return <h1>我是首页组件</h1>;
}

function MyMusic() {
  return <h1>我是我的音乐件</h1>;
}

function Friend() {
  return <h1>我是朋友组件</h1>;
}
