import React from 'react';

export default function Footer({ type, handleType }) {
  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>1</strong> 剩余
      </span>
      <ul className="filters">
        <li>
          <a
            //9.4 点击时调用函数，并且传参
            onClick={() => handleType('all')}
            // 9.2. 做排它判断
            className={type === 'all' ? 'selected' : ''}
            href="#/"
          >
            全部
          </a>
        </li>
        <li>
          <a
            //9.4 点击时调用函数，并且传参
            onClick={() => handleType('active')}
            // 9.2. 做排它判断
            className={type === 'active' ? 'selected' : ''}
            href="#/active"
          >
            未完成
          </a>
        </li>
        <li>
          <a
            //9.4 点击时调用函数，并且传参
            onClick={() => handleType('completed')}
            // 9.2. 做排它判断
            className={type === 'completed' ? 'selected' : ''}
            href="#/completed"
          >
            已完成
          </a>
        </li>
      </ul>
      <button className="clear-completed">清除已完成</button>
    </footer>
  );
}
