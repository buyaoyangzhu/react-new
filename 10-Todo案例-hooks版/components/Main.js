import React from 'react';

// 2.3. Main组件中通过props接收list
export default function Main({ list, handleUpdateById, handleDelById, isAll, handleCheckAll }) {
  return (
    <section className="main">
      <input
        // 7.3 通过isAll控制checked属性
        checked={isAll}
        onChange={handleCheckAll}
        id="toggle-all"
        className="toggle-all"
        type="checkbox"
      />
      <label htmlFor="toggle-all">全选</label>
      <ul className="todo-list">
        {/* 2.4. 列表渲染 */}
        {list.map((item) => {
          return (
            <li
              key={item.id}
              // 2.5. 处理className和checked属性
              className={item.isDone ? 'completed' : ''}
            >
              <div className="view">
                <input
                  className="toggle"
                  type="checkbox"
                  // 2.5. 处理className和checked属性
                  checked={item.isDone}
                  // 3.3 子组件Main中调用函数，传参id
                  onChange={() => handleUpdateById(item.id)}
                />
                <label>{item.task}</label>
                <button
                  className="destroy"
                  // 4.3 Main中点击事件调用函数，传参id
                  onClick={() => handleDelById(item.id)}
                ></button>
              </div>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
