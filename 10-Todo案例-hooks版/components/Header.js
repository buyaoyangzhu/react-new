import React, { useState } from 'react';

export default function Header({ handleAdd }) {
  // 5.1 Header组件改造为受控组件

  const [task, setTask] = useState('');

  const handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      // 5.2 监听回车事件
      // console.log('回车事件触发了  ----->  ');
      // 5.5 在Header组件的回车事件中，触发调函数
      handleAdd(task);
      // 5.7 清空输入框的值
      setTask('');
    }
  };

  return (
    <header className="header">
      <h1>todos</h1>
      <input
        className="new-todo"
        placeholder="需要做什么"
        autoFocus
        value={task}
        onChange={(e) => setTask(e.target.value)}
        onKeyDown={handleKeyDown}
      />
    </header>
  );
}
