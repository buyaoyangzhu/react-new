export const TODO_ADD = 'todo/add';
export const TODO_UPDATE = 'todo/update';
export const TODO_CHECKALL = 'todo/checkAll';
export const TODO_DEL = 'todo/del';
export const USER_UPDATE = 'user/update';
