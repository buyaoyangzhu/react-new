/*
  学习目标：Action设计
  步骤：
     1. 分模块文件去维护action创建函数
     2. actionType的值，分两层："功能模块/具体功能": "todo/update" 、 "user/update" "todo/del"
     3. 使用actionTypes文件，维护所有type常量
*/

import { TODO_ADD, TODO_CHECKALL, TODO_DEL, TODO_UPDATE } from './actionType';

export const updateByIdAction = (id) => {
  return {
    type: TODO_UPDATE,
    payload: id,
  };
};

export const delByIdAction = (id) => {
  return {
    type: TODO_DEL,
    payload: id,
  };
};

export const addAction = (newTaskStr) => {
  const newTask = { id: Date.now(), isDone: false, task: newTaskStr };
  return {
    type: TODO_ADD,
    payload: newTask,
  };
};

export const checkAllAction = (checked) => ({
  type: TODO_CHECKALL,
  payload: checked,
});
