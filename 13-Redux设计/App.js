/*
  学习目标：Redux设计-小结
  1. 文件结构：
    1.1. store/index.js 负责创建store
    1.2. store/reducer 负责创建reducer、合并reducer
    1.3 store/action 负责创建action函数、actionType
  2. Reducer设计
    2.1 分离：分功能模块拆分文件， 如： reducer/todo.js reducer/user.js
    2.2 合并：reducer/index.js 合并模块： combineReducers({ 功能模块名： xxxReducer})
    2.2 命名：按功能模块命名： todoReducer  userReducer
  3. Action设计
    3.1 文件：按功能拆分，如action/todo.js  action/user.js
    3.2 type的值：按"模块名/具体功能名"命名
    3.3 actioType.js ：用常量维护type的值
*/

import React from 'react';
import Footer from './components/Footer';
import Header from './components/Header';
import Main from './components/Main';
import './styles/base.css';
import './styles/index.css';

export default function App() {
  return (
    <section className="todoapp">
      <Header />
      <Main />
      <Footer />
    </section>
  );
}
