/*
  学习目标：掌握props传递数据-class组件 、 函数组件
  props: 所有标签属性组成的对象
  作用：给组件传递数据
*/

import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        {/* 1. 传递数据: 给标签加上标签属性和值 */}
        <Child name="zs123" age={18123} gender={'xxxx'}></Child>
        <Child2 name="zs2" age={28} gender={'xxxx'}></Child2>
      </div>
    );
  }
}

// 2. 接收数据
// 函数式组件：通过形参接收props
// 👍 推荐解构props对象
function Child({ name, age }) {
  // console.log('props  ----->  ', props);
  return (
    <h1>
      我是子组件 - {name} -{age}
    </h1>
  );
}

// 2. 接收数据
// 类组件：通过this.props接收
class Child2 extends React.Component {
  render() {
    console.log('this  ----->  ', this);
    const { name, age } = this.props;
    return (
      <div>
        <h1>{name}</h1>
        <h1>{age}</h1>
      </div>
    );
  }
}
