/*
  学习目标：通过状态提升-实现兄弟组件通信
  需求：能够让Wife花老公的钱
 
*/

import React, { Component } from 'react';
import Husband from './components/Husband';
import Wife from './components/Wife';

export default class App extends Component {
  //  1. 把Husband的state挪到APp组件中
  state = {
    money: 0,
  };

  // 4. 子传父给HUsband：父组件内定义一个赚钱的方法
  /* 赚钱方法 */
  handleMakeMoney = () => {
    this.setState({ money: this.state.money + 1000 });
  };

  // 6. 子传父给Wife： 定义一个花钱的方法
  handleCostMoney = () => {
    this.setState({ money: this.state.money - 5000 });
  };

  render() {
    return (
      <div>
        <h1 style={{ textAlign: 'center' }}>家庭存款：</h1>
        <Husband
          // 2. 通过父传子，将moneny传给Husband
          money={this.state.money}
          // 5. 将函数传给子组件，子组件负责调用
          handleMakeMoney={this.handleMakeMoney}
        ></Husband>
        <hr />
        <Wife handleCostMoney={this.handleCostMoney}></Wife>
      </div>
    );
  }
}
