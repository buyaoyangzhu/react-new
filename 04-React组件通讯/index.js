/*
  学习目标：评论列表静态结构准备
  步骤：
     1. src/index.js 项目入口文件
     2. src/App.js 根组件文件
     3. src/index.css 样式文件
*/

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(<App></App>, document.getElementById('root'));
