/*
  学习目标：子传父技术- 子组件可以花钱
  💥 父组件，传一个函数给子组件
  💥 子组件：调用父组件传来的函数

*/

import React from 'react';

export default class Parent extends React.Component {
  state = {
    money: 1000,
  };

  handleMakeMoney = () => {
    this.setState({
      money: 1000 + this.state.money,
    });
  };

  // 1. 父组件内定义一个函数
  handleCost = (num) => {
    console.log('num  ----->  ', num);
    this.setState({ money: this.state.money - num });
  };

  render() {
    return (
      <div>
        <button onClick={this.handleMakeMoney}>爸爸开始赚钱了</button>
        <Child
          money={this.state.money}
          // 2. 通过props传函数给子组件
          handleCost={this.handleCost}
        ></Child>
      </div>
    );
  }
}

class Child extends React.Component {
  render() {
    const { money, handleCost } = this.props;
    return (
      <div>
        <h1>爸爸给我钱了： {money} </h1>

        {/* 3. 子组件：调用父组件传来的函数 */}
        <button onClick={() => handleCost(8000)}>点我花爸爸的钱</button>
      </div>
    );
  }
}
