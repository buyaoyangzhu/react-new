/*
  学习目标：父传子技术
  步骤：
     1. 给子组件标签设置标签数据
     2. 子组件通过props去接收
*/

import React from 'react';

export default class Parent extends React.Component {
  state = {
    money: 1000,
  };

  handleMakeMoney = () => {
    this.setState({
      money: 1000 + this.state.money,
    });
  };

  render() {
    return (
      <div>
        <button onClick={this.handleMakeMoney}>爸爸开始赚钱了</button>
        <Child money={this.state.money}></Child>
      </div>
    );
  }
}

class Child extends React.Component {
  render() {
    // cp
    const { money } = this.props;
    return (
      <div>
        <h1>爸爸给我钱了： {money} </h1>
      </div>
    );
  }
}
