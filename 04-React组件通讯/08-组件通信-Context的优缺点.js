/* 
  目标：Context优缺点
  缺点：🔔增加组件的嵌套解构，难以阅读和理解
  优点：
    1. 🔔是React自带的跨组件通信方案，不需要额外下包
    2. 第三方的包喜欢用这个，减少体积
  场景🔔：
    1. 多语言切换
    2. 一键换色

*/

import React from 'react';

const { Provider, Consumer } = React.createContext();
// 1. 可以多次调用createContext
const { Provider: Provider2, Consumer: Consumer2 } = React.createContext();
console.log(' Provider2 ----->  ', Provider2, Consumer2);

export default class App extends React.Component {
  render() {
    return (
      <div>
        <h1>父组件</h1>
        <Provider value="hello 我跨组件而来">
          <Son></Son>
        </Provider>
      </div>
    );
  }
}

class Son extends React.Component {
  render() {
    return (
      <div>
        <h2> 儿子</h2>
        <Provider2 value="Son传来的数据">
          <SonSon></SonSon>
        </Provider2>
      </div>
    );
  }
}

class SonSon extends React.Component {
  render() {
    return (
      <div>
        <h2> 孙子</h2>
        <SonSonSon></SonSonSon>
        {/* 💥 接受一个函数，返回需要返回一端JSX，形参就是Provider传来的数据 */}
        <Consumer>
          {(data) => {
            return <h1>我是Consumer - {data}</h1>;
          }}
        </Consumer>
      </div>
    );
  }
}

class SonSonSon extends React.Component {
  render() {
    return (
      <div>
        <h2> 孙子的儿子</h2>
        {/* 2. 成对儿使用Provider和Consumer */}
        <Consumer2>
          {(data) => {
            return <i>{data}</i>;
          }}
        </Consumer2>
      </div>
    );
  }
}
