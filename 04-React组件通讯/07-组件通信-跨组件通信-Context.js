/* 
  目标： 使用Context来跨组价通信，让App组件-直接传数据给SonSon组件

*/

import React from 'react';

// 1. React.createContext创建上下文, 解构出两个组件
const { Provider, Consumer } = React.createContext();

export default class App extends React.Component {
  render() {
    return (
      <div>
        <h1>父组件</h1>
        <Provider
          // 2. 使用Provider组件，传数据: 设置value属性传数据
          // 💥包住后代
          value="hello 我跨组件而来"
        >
          <Son></Son>
        </Provider>
      </div>
    );
  }
}

class Son extends React.Component {
  render() {
    return (
      <div>
        <h2> 儿子</h2>
        <SonSon></SonSon>
      </div>
    );
  }
}

class SonSon extends React.Component {
  render() {
    return (
      <div>
        <h2> 孙子</h2>
        {/* 3. 使用Consumer组件，接收数据 */}
        {/* 💥 接受一个函数，返回需要返回一端JSX，形参就是Provider传来的数据 */}
        <Consumer>
          {(data) => {
            return <h1>我是Consumer - {data}</h1>;
          }}
        </Consumer>
      </div>
    );
  }
}
