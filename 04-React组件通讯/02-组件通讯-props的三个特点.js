/*
  学习目标：props的三个特点
  步骤：
     1. props不必先定义后使用
     2. props可以传递任意数据类型
          number string null undefined boolean object
          🔔 function  JSX
     3. props是只读的 - 类似vue中的单项数据流
*/

import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        {/* 1. props不必先定义后使用 */}
        <Child name="zs123" age={18123} gender={'xxxx'}></Child>
        {/* null undefined boolean  */}
        <Child name={null} age={undefined} gender={true}></Child>
        {/* object  */}
        <Child person={{ zs: 'zs' }}></Child>
        {/* 数组 */}
        <Child list={[1, 2, 3, 4]}></Child>
        {/* 🔔 function */}
        <Child fn={() => alert(123)}></Child>
        {/* 🔔JSX  */}
        <Child msg={<i>我是props传来的JSX</i>}></Child>
      </div>
    );
  }
}

function Child({ msg, name, age, gender, person, list, fn }) {
  console.log('fn  ----->  ', fn);
  return (
    <div>
      {/* number string  */}
      我是子组件 - {String(name)} - {age}
      <h1>{gender}</h1>
      {JSON.stringify(person)}
      <h2>{list}</h2>
      <button onClick={fn}>点我</button>
      {msg}
      <button
        onClick={() => {
          msg = '123';
        }}
      >
        点我修改props
      </button>
    </div>
  );
}

// 1. props
