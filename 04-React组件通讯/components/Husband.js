import React, { Component } from 'react';

export default class Husband extends Component {
  render() {
    return (
      <div style={{ padding: '10px', border: '10px solid #ccc' }}>
        {/* 3. 通过props访问money */}
        <h1>老公的钱: {this.props.money}</h1>
        <button onClick={this.props.handleMakeMoney}>老公赚钱</button>
      </div>
    );
  }
}
