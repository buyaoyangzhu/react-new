/*
  学习目标：props校验
  作用：指定标签属性的类型，违反则报错, 类似Vue中props的对象写法
*/

import React, { Component } from 'react';

// 1. 导入包props-type，提供类型
import PropTypes from 'prop-types';
// 3. 浏览器中验证效果

export default class App extends Component {
  render() {
    return (
      <div>
        <Child msg="123" isShow={false} zs={{ name: 'zs', age: 18 }}></Child>
      </div>
    );
  }
}

class Child extends React.Component {
  render() {
    return <div></div>;
  }
}

// 2. 给组件设置规则对象
// 💥 组件名.propTypes = { 规则}
Child.propTypes = {
  msg: PropTypes.string.isRequired,
  list: PropTypes.array,
  person: PropTypes.object,
  isShow: PropTypes.bool,
  handleClick: PropTypes.func,
  // 💥 JSX
  title: PropTypes.element,
  zs: PropTypes.shape({
    name: PropTypes.string,
    age: PropTypes.number,
  }),
};
