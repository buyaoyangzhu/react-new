/*
  学习目标：ES6中static关键字的使用
*/

import React, { Component } from 'react';

import PropTypes from 'prop-types';

export default class App extends Component {
  render() {
    return (
      <div>
        <Child msg="123" isShow={false} zs={{ name: 'zs', age: 18 }}></Child>
      </div>
    );
  }
}

class Child extends React.Component {
  static propTypes = {
    msg: PropTypes.string.isRequired,
    list: PropTypes.array,
    person: PropTypes.object,
    isShow: PropTypes.bool,
    handleClick: PropTypes.func,
    // 💥 JSX
    title: PropTypes.element,
    zs: PropTypes.shape({
      name: PropTypes.string,
      age: PropTypes.number,
    }),
  };

  static defaultProps = {
    hello: '123',
  };

  render() {
    return <div>{this.props.hello}</div>;
  }
}
