/*
  学习目标：props指定默认值
*/

import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        <Child></Child>
        <Son></Son>
      </div>
    );
  }
}

class Child extends React.Component {
  render() {
    return <div>{this.props.msg}</div>;
  }
}
Child.defaultProps = {
  msg: 'hello Child',
};

// 👍函数组件的第二种指定方法
function Son({ msg = 'hello Son' }) {
  return <h1>{msg}</h1>;
}

// 🔔 函数组件的第一种指定方法
// Son.defaultProps = {
//   msg: 'hello Son',
// };
