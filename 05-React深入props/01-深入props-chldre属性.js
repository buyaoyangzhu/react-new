/*
  学习目标：深入props-chldre属性
  类似：vue中的插槽
  本质：组件标签夹着的内容区域
  特点：
    childre属性可以是任意数据类型，与props的其它属性没有区别

*/

import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        {/* <Child>456</Child> */}
        {/* <Child>{789}</Child> */}

        {/* null undefined boolean */}
        {/* <Child>{null}</Child>
        <Child>{undefined}</Child>
        <Child>{true}</Child> */}

        {/* 对象  */}
        {/* <Child>{{ name: 'zs' }}</Child> */}

        {/* 数组  */}
        {/* <Child>{[1, 2, 3, 4]}</Child> */}

        {/* 💥函数  */}
        <Child2>
          {() => {
            return <i>我是children传来的函数</i>;
          }}
        </Child2>
        {/* 💥JSX */}
        <Child>
          <ul>
            <li>123</li>
            <li>456</li>
          </ul>
        </Child>
      </div>
    );
  }
}

function Child2({ children }) {
  return <h1>我是CHild - {children()}</h1>;
}

function Child({ children }) {
  return <h1>{children}</h1>;
}
