/*
  学习目标：使用children属性，模拟Vue中的插槽
*/

import React, { Component } from 'react';

export default class App extends Component {
  state = {
    msg: 'hello React',
  };

  render() {
    return (
      <div>
        {/* 匿名插槽 */}
        <Child>{this.state.msg}</Child>

        {/* 具名插槽 */}
        <ChildHasName>
          {{
            header: '蜀道难',
            footer: '难于上青天',
          }}
        </ChildHasName>
        <ChildHasName>
          {{
            header: '鹅鹅鹅',
            footer: '红掌拨清波',
          }}
        </ChildHasName>

        {/* 作用域插槽 */}
        <ChildScope>
          {(data) => {
            return (
              <ul>
                {data.map((item) => {
                  return <li key={item}>{item}</li>;
                })}
              </ul>
            );
          }}
        </ChildScope>
        <ChildScope>
          {(data) => (
            <div>
              {data.map((item) => (
                <i key={item}>{item}</i>
              ))}
            </div>
          )}
        </ChildScope>
      </div>
    );
  }
}

class ChildScope extends Component {
  state = {
    list: ['苹果', '橘子', '香蕉'],
  };

  render() {
    return <h1>Child子组件 - {this.props.children(this.state.list)}</h1>;
  }
}

class ChildHasName extends Component {
  render() {
    return (
      <div>
        <header>显示header： {this.props.children.header}</header>
        <footer>显示footer: {this.props.children.footer}</footer>
      </div>
    );
  }
}

class Child extends Component {
  render() {
    console.log('this  ----->  ', this);
    return <h1>Child子组件 - {this.props.children}</h1>;
  }
}
