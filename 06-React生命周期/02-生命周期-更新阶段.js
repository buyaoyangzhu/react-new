/*
  学习目标：生命周期 - 更新阶段
  三个阶段
     1. 挂载时
     2. 更新时
     3. 卸载时
*/
import React, { Component } from 'react';

// React中触发组件更新，有两种方式
// 1. setState
// 2. 🔔props变化

// 💥 注意：
// 1. 更新阶段的钩子函数中，不能调用setState会造成死循环
// 2. React中的更新指的是数据更新，Vue中的更新阶段指的是DOM更新

export default class App extends Component {
  state = {
    count: 100,
  };

  // 1. 作用：重新渲染界面
  render() {
    // console.log('render触发了，重新渲染界面  ----->  ');
    return (
      <div>
        <h1>App - {this.state.count}</h1>
        <Child count={this.state.count}></Child>
        <button
          onClick={() => {
            this.setState({ count: this.state.count + 1 });
          }}
        >
          点我修改count
        </button>
      </div>
    );
  }

  // 2. 代表：更新后，对标Vue中的updated，类似vue中的watch
  // 场景：💥做缓存
  // 可以获取到更新后的数据
  // cdup
  componentDidUpdate() {
    console.log('this.state.count  ----->  ', this.state.count);
    // console.log('componentDidUpdate触发了，表示更新后  ----->  ');
  }
}

class Child extends React.Component {
  componentDidUpdate() {
    console.log('props改变，触发了子组的更新阶段 ----->  ');
  }
  render() {
    console.log('rops改变，触发了子组的更新阶段  ----->  ');
    return <div>{this.props.count}</div>;
  }
}

// render

// componentDidUpdate
