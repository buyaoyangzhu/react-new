/*
  学习目标：生命周期 - 卸载阶段
  三个阶段
     1. 挂载时
     2. 更新时
     3. 卸载时
*/

import React, { Component } from 'react';

export default class App extends Component {
  state = {
    isShow: true,
  };

  render() {
    const { isShow } = this.state;
    return <div>{isShow && <Son></Son>}</div>;
  }
}

class Son extends React.Component {
  // 类比：vue中的beforeDestroy
  // 🔔场景：做清除的动作，如：关闭定时器，取消监听的事件
  componentWillUnmount() {
    console.log('我是子组件，我要被卸载掉了  ----->  ');
  }

  render() {
    return <div>Son</div>;
  }
}
