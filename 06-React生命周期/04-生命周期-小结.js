/*
  学习目标：生命周期-小结
  三大阶段：
    1. 挂载阶段 - 
      1.1 constructor: 创建时，对标vue中的 created
          💥 早期声明state和ref，现在都简写
      1.2 render：渲染dom
      1.3 componentDidMount: 挂载后，对标vue中的mounted
          💥 3.1 发请求、最先能够获取DOM、定时器、绑定事件
    2. 更新阶段
      2.1 render： 重新渲染dom
      2.2 compoentDidUpdate：更新后，对标vue中的updated
         💥 可以获取到更新后的值，常用来做缓存
    3. 卸载阶段 -
      3.1 componentWillUnMount
        💥 做清除的动作，如：清除定时器、取消监听的事件

    执行次数：
      constructor和componentDidMount只会执行一次
      componentWillUnMount只会执行一次
*/

import React, { Component } from 'react';

export default class App extends Component {
  state = {
    isShow: true,
    count: 0,
  };

  render() {
    const { isShow, count } = this.state;
    return (
      <div>
        {isShow && <Child count={count}></Child>}

        <button
          onClick={() => {
            this.setState({ isShow: !isShow });
          }}
        >
          点我卸载子组件
        </button>
        <button
          onClick={() => {
            this.setState({ count: this.state.count + 1 });
          }}
        >
          点我更新count
        </button>
      </div>
    );
  }
}

class Child extends React.Component {
  constructor() {
    super();
    console.log('constructor执行了  ----->  ');
  }

  render() {
    console.log('render执行了  ----->  ');
    return <div>{this.props.count}</div>;
  }

  componentDidMount() {
    console.log(' componentDidMount执行了 ----->  ');
  }

  componentDidUpdate() {
    console.log('componentDidUpdate执行了  ----->  ');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount执行了  ----->  ');
  }
}
