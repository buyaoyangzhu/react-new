import React, { Component } from 'react';

/*
  学习目标：React生命周期-挂载阶段
  三大阶段：挂载时、更新时、卸载时
  挂载时 等同于Vue中的创建和挂载二合一
  
*/

export default class App extends Component {
  // 1. 代表创建时，对标vue中的beforeCreate 和created
  // 作用：（早期）声明state和ref
  constructor() {
    // 💥 一定要调用super()，表示继承父级的一切方法
    super();
    this.state = {
      count: 100,
    };
    this.iptRef = React.createRef();

    console.log('1  ----->  ', 1);
  }

  // 2. 作用：表示渲染什么内容
  render() {
    console.log('2  ----->  ', 2);
    return <h1>App - {this.state.count}</h1>;
  }

  // 3.代表：挂载后 对标的Vue中的mounted，
  // 场景：💥1.发送请求、2. 最先获取DOM， 3监听、开启定时器等
  componentDidMount() {
    const h1Node = document.querySelector('h1');
    console.log('h1Node  ----->  ', h1Node);
    // h1Node.addEventListener();
    console.log('3  ----->  ', 3);
  }
}
